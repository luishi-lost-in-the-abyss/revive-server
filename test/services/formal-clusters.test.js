const app = require("../../src/app");

describe("'formal-clusters' service", () => {
  it("registered the service", () => {
    const service = app.service("formal-clusters");
    expect(service).toBeTruthy();
  });
});
