const app = require("../../src/app");

describe("'equipment' service", () => {
  it("registered the service", () => {
    const service = app.service("equipment");
    expect(service).toBeTruthy();
  });
});
