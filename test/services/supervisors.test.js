const app = require("../../src/app");

describe("'supervisors' service", () => {
  it("registered the service", () => {
    const service = app.service("supervisors");
    expect(service).toBeTruthy();
  });
});
