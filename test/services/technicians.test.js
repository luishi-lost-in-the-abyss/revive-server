const app = require("../../src/app");

describe("'technicians' service", () => {
  it("registered the service", () => {
    const service = app.service("technicians");
    expect(service).toBeTruthy();
  });
});
