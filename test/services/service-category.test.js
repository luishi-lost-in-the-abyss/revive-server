const app = require("../../src/app");

describe("'service-category' service", () => {
  it("registered the service", () => {
    const service = app.service("service-category");
    expect(service).toBeTruthy();
  });
});
