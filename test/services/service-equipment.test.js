const app = require("../../src/app");

describe("'service-equipment' service", () => {
  it("registered the service", () => {
    const service = app.service("service-equipment");
    expect(service).toBeTruthy();
  });
});
