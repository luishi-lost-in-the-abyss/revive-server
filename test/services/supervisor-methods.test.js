const app = require("../../src/app");

describe("'supervisor-methods' service", () => {
  it("registered the service", () => {
    const service = app.service("supervisor-methods");
    expect(service).toBeTruthy();
  });
});
