const app = require("../../src/app");

describe("'dispatchers' service", () => {
  it("registered the service", () => {
    const service = app.service("dispatchers");
    expect(service).toBeTruthy();
  });
});
