const csv = require("csvtojson");
const iconvlite = require("iconv-lite");

module.exports = async function({ app, update }) {
  //setup locations if not exist
  const southCotabatoFile = await csv().fromFile(
    "./data/locations/south-cotabato.csv"
  );
  const northCotabatoFile = await csv().fromFile(
    "./data/locations/north-cotabato.csv"
  );
  const sultanKudaratFile = await csv().fromFile(
    "./data/locations/sultan-kudarat.csv"
  );
  const regionXiiFile = [
    ...southCotabatoFile,
    ...northCotabatoFile,
    ...sultanKudaratFile
  ];
  return app
    .service("location-country")
    .find({})
    .then(async res => {
      if (res.total > 0) {
        update && update();
        return;
      }
      const country = await app
        .service("location-country")
        .create({ name: "Philippines" });

      const regionObj = {};
      const provinceObj = {};
      const municipalityObj = {};
      const barangayPromises = [];
      for (i in regionXiiFile) {
        const { region, province, municipality, barangay } = regionXiiFile[i];
        if (!regionObj[region]) {
          regionObj[region] = await app.service("location-region").create({
            name: iconvlite.encode(region, "win1251"),
            countryId: country.id
          });
          regionObj[region] = regionObj[region].id;
        }

        if (!provinceObj[province]) {
          provinceObj[province] = await app
            .service("location-province")
            .create({
              name: iconvlite.encode(province, "win1251"),
              regionId: regionObj[region]
            });
          provinceObj[province] = provinceObj[province].id;
        }

        if (!municipalityObj[municipality]) {
          municipalityObj[municipality] = await app
            .service("location-municipality")
            .create({
              name: iconvlite.encode(municipality, "win1251"),
              provinceId: provinceObj[province]
            });
          municipalityObj[municipality] = municipalityObj[municipality].id;
        }

        barangayPromises.push(
          app.service("location-barangay").create({
            name: iconvlite.encode(barangay, "win1251"),
            municipalityId: municipalityObj[municipality]
          })
        );
      }
      await Promise.all(barangayPromises);
      update && update();
    });
};
