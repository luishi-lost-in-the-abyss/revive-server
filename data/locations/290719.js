const csv = require("csvtojson");
const iconvlite = require("iconv-lite");

module.exports = async function({ app, update }) {
  const checkIfShouldProceed = await app.service("location-province").find({});
  if (checkIfShouldProceed.total > 3) {
    // should only have 3 provinces at this point of time to apply this patch
    update && update();
    return;
  }
  console.log("Loading location patch 290719");

  //setup locations if not exist
  const saranganiFile = await csv().fromFile("./data/locations/sarangani.csv");
  const regionXiiFile = [...saranganiFile];

  const maguindanaoFile = await csv().fromFile(
    "./data/locations/maguindanao.csv"
  );
  const armmRegionFile = [...maguindanaoFile];

  const country = await app.service("location-country").get(1); //this should be philippines
  if (country.name !== "Philippines")
    throw new Error("Database Corrupted - Country incorrect");

  const regionXi = await app.service("location-region").get(1);
  if (regionXi.name !== "REGION XII (Soccsksargen)")
    throw new Error("Database Corrupted - Region incorrect");

  // load up sarangani barangays and provinces;
  async function loadRegionXi() {
    const provinceObj = {};
    const municipalityObj = {};
    const barangayPromises = [];
    for (i in regionXiiFile) {
      const { province, municipality, barangay } = regionXiiFile[i];

      if (!provinceObj[province]) {
        provinceObj[province] = await app.service("location-province").create({
          name: iconvlite.encode(province, "win1251"),
          regionId: regionXi.id
        });
        provinceObj[province] = provinceObj[province].id;
      }

      if (!municipalityObj[municipality]) {
        municipalityObj[municipality] = await app
          .service("location-municipality")
          .create({
            name: iconvlite.encode(municipality, "win1251"),
            provinceId: provinceObj[province]
          });
        municipalityObj[municipality] = municipalityObj[municipality].id;
      }

      barangayPromises.push(
        app.service("location-barangay").create({
          name: iconvlite.encode(barangay, "win1251"),
          municipalityId: municipalityObj[municipality]
        })
      );
    }

    return Promise.all(barangayPromises);
  }

  async function loadArmmRegion() {
    const regionObj = {};
    const provinceObj = {};
    const municipalityObj = {};
    const barangayPromises = [];
    for (i in armmRegionFile) {
      const { region, province, municipality, barangay } = armmRegionFile[i];
      if (!regionObj[region]) {
        regionObj[region] = await app.service("location-region").create({
          name: iconvlite.encode(region, "win1251"),
          countryId: country.id
        });
        regionObj[region] = regionObj[region].id;
      }

      if (!provinceObj[province]) {
        provinceObj[province] = await app.service("location-province").create({
          name: iconvlite.encode(province, "win1251"),
          regionId: regionObj[region]
        });
        provinceObj[province] = provinceObj[province].id;
      }

      if (!municipalityObj[municipality]) {
        municipalityObj[municipality] = await app
          .service("location-municipality")
          .create({
            name: iconvlite.encode(municipality, "win1251"),
            provinceId: provinceObj[province]
          });
        municipalityObj[municipality] = municipalityObj[municipality].id;
      }

      barangayPromises.push(
        app.service("location-barangay").create({
          name: iconvlite.encode(barangay, "win1251"),
          municipalityId: municipalityObj[municipality]
        })
      );
    }
    return Promise.all(barangayPromises);
  }
  try {
    await Promise.all([loadRegionXi(), loadArmmRegion()]);
  } catch (err) {
    console.log(err);
  }
  update && update();
};
