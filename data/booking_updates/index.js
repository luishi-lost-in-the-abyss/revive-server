const firstUpdate = require("./170919.js");

module.exports = async function({ app }) {
  if (app.get("skipBookingUpdates")) return;
  try {
    await firstUpdate({
      app,
      update: null
    });
  } catch (err) {
    console.log(err);
  }
};
