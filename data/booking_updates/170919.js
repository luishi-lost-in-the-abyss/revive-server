const { DateTime } = require("luxon");
const TIMEZONE = "Asia/Singapore";
const toUpdateBookingIds = [48, 49, 50, 51, 52, 53, 54, 55, 60, 61, 67, 68, 69];
module.exports = async function({ app, update }) {
  return app
    .service("bookings")
    .find({
      paginate: false,
      query: {
        id: { $in: toUpdateBookingIds }
      }
    })
    .then(async res => {
      // should check if last updated is above the date else don't fix
      let checkDate = res[0].updatedAt;
      checkDate = DateTime.fromJSDate(checkDate).setZone(TIMEZONE);

      const updateDate = DateTime.local()
        .setZone(TIMEZONE)
        .set({ hour: 15, minute: 27, day: 17, month: 9, year: 2019 });

      if (checkDate > updateDate) {
        // don't apply patch already after date
        update && update();
        return;
      }
      console.log("APPLYING 170919 BOOKING UPDATES");

      const updateObj = {};
      res.map(({ id, ...obj }) => {
        updateObj[id] = obj.dataValues;
      });

      let updating = updateObj[48],
        updatingValues = {
          startDate: DateTime.fromJSDate(updating.startDate)
            .setZone(TIMEZONE)
            .set({ day: 13, month: 7, year: 2019 })
            .toJSDate(),
          endDate: DateTime.fromJSDate(updating.endDate)
            .setZone(TIMEZONE)
            .set({ day: 13, month: 7, year: 2019 })
            .toJSDate(),
          serviceId: (await app
            .service("services")
            .find({ paginate: false, query: { code: "WH-2" } }))[0].id
        };
      await app.service("bookings").patch(48, updatingValues);

      updating = updateObj[49];
      updatingValues = {
        startDate: DateTime.fromJSDate(updating.startDate)
          .setZone(TIMEZONE)
          .set({ hour: 13, minute: 0 })
          .toJSDate(),
        endDate: DateTime.fromJSDate(updating.endDate)
          .setZone(TIMEZONE)
          .set({ hour: 14 })
          .endOf("hour")
          .toJSDate()
      };
      await app.service("bookings").patch(49, updatingValues);

      updating = updateObj[50];
      updatingValues = {
        startDate: DateTime.fromJSDate(updating.startDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        endDate: DateTime.fromJSDate(updating.endDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        serviceArea: 0.5
      };
      await app.service("bookings").patch(50, updatingValues);

      updating = updateObj[51];
      updatingValues = {
        startDate: DateTime.fromJSDate(updating.startDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        endDate: DateTime.fromJSDate(updating.endDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        serviceArea: 0.5
      };
      await app.service("bookings").patch(51, updatingValues);

      updating = updateObj[52];
      updatingValues = {
        startDate: DateTime.fromJSDate(updating.startDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        endDate: DateTime.fromJSDate(updating.endDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        serviceArea: 1
      };
      await app.service("bookings").patch(52, updatingValues);

      updating = updateObj[53];
      updatingValues = {
        startDate: DateTime.fromJSDate(updating.startDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        endDate: DateTime.fromJSDate(updating.endDate)
          .setZone(TIMEZONE)
          .set({ day: 12, month: 7, year: 2019 })
          .toJSDate(),
        serviceArea: 1
      };
      await app.service("bookings").patch(53, updatingValues);

      updating = updateObj[54];
      updatingValues = {
        serviceArea: 1
      };
      await app.service("bookings").patch(54, updatingValues);

      updating = updateObj[55];
      updatingValues = {
        serviceArea: 1
      };
      await app.service("bookings").patch(55, updatingValues);

      updating = updateObj[60];
      updatingValues = {
        serviceArea: 0.5
      };
      await app.service("bookings").patch(60, updatingValues);

      updating = updateObj[61];
      updatingValues = {
        serviceArea: 0.5
      };
      await app.service("bookings").patch(61, updatingValues);

      updating = updateObj[67];
      updatingValues = {
        serviceArea: 0.5
      };
      await app.service("bookings").patch(67, updatingValues);

      updating = updateObj[68];
      updatingValues = {
        serviceArea: 1.0
      };
      await app.service("bookings").patch(68, updatingValues);

      updating = updateObj[69];
      updatingValues = {
        farmPlotId: 805
      };
      await app.service("bookings").patch(69, updatingValues);

      const dataSnapshotUpdates = toUpdateBookingIds.map(async id => {
        return app
          .service("bookings")
          .get(id)
          .then(async booking => {
            let participants = booking.farm_plot.formal_cluster
              ? [
                  await app
                    .service("formal-clusters")
                    .get(booking.farm_plot.formal_cluster.id)
                ]
              : await app.service("participants").find({
                  paginate: false,
                  query: booking.farm_plot.clusterId
                    ? {
                        clusterId: booking.farm_plot.clusterId
                      }
                    : { id: booking.farm_plot.participant.id }
                });
            await app
              .service("bookings")
              .patch(booking.id, { dataSnapshot: { booking, participants } });
          });
      });

      await Promise.all(dataSnapshotUpdates);

      updating.update && update();
    });
};
