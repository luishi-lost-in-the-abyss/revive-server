const jwt = require("jsonwebtoken");
const errors = require("@feathersjs/errors");
const path = require("path");

module.exports = function(app) {
  return async function(req, res, next) {
    const checkForAuth = app.get("staticAuthentication");
    if (checkForAuth) {
      const cookie = req.header("cookie");
      const authentication = app.get("authentication");
      const tokenCookie = cookie
        ? cookie.replace(
            /(?:(?:^|.*;\s*)revive-jwt\s*\=\s*([^;]*).*$)|^.*$/,
            "$1"
          )
        : null;
      let decoded;
      try {
        decoded = jwt.verify(tokenCookie, authentication.secret);
      } catch (err) {
        return next(new errors.NotAuthenticated());
      }
    }
    return res.download(`./uploads/${req.url.split("?")[0]}`);
  };
};
