const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const disablePagination = require("../../hooks/disablePagination");

const innerJoinFunction = function(context) {
  const Users = context.app.services["users"].Model;
  const Fec = context.app.services["fec"].Model;

  context.params.sequelize = {
    raw: false,
    include: [
      {
        model: Users,
        attributes: ["email", "username"]
      },
      { model: Fec, attributes: ["name"] }
    ]
  };
  return context;
};

const adminOnly = authRole({ super_admin: true, admin: true });
module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({ super_admin: true, admin: true, dispatcher: true })
    ],
    find: [innerJoinFunction],
    get: [innerJoinFunction],
    create: [adminOnly],
    update: [adminOnly],
    patch: [adminOnly],
    remove: [adminOnly]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
