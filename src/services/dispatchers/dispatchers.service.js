// Initializes the `dispatchers` service on path `/dispatchers`
const createService = require("feathers-sequelize");
const createModel = require("../../models/dispatchers.model");
const hooks = require("./dispatchers.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/dispatchers", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("dispatchers");

  service.hooks(hooks);
};
