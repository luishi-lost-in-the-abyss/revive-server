const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const disablePagination = require("../../hooks/disablePagination");

const adminOnly = authRole({ super_admin: true, admin: true });

const innerJoinFunction = function(context) {
  const Users = context.app.services["users"].Model;
  const Technicians = context.app.services["technicians"].Model;
  const Fec = context.app.services["fec"].Model;

  context.params.sequelize = {
    raw: false,
    include: [
      {
        model: Users,
        attributes: ["email", "username"]
      },
      { model: Technicians },
      { model: Fec }
    ]
  };
  return context;
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({ super_admin: true, admin: true, supervisor: true })
    ],
    find: [disablePagination, innerJoinFunction],
    get: [innerJoinFunction],
    create: [adminOnly],
    update: [disallow("external")],
    patch: [adminOnly],
    remove: [
      adminOnly,
      async function(context) {
        // find replacement supervisor in same fec
        const supervisor = await context.app
          .service("supervisors")
          .get(context.id);
        if (supervisor.technicians.length === 0) {
          //no people under him just delete
          return context;
        }
        const replacement = await context.app.service("supervisors").find({
          query: { fecId: supervisor.fecId, id: { $ne: context.id } }
        });
        if (replacement.total <= 0) {
          throw new errors.BadRequest("no replacement available");
        }
        await context.app
          .service("technicians")
          .patch(
            null,
            { supervisorId: replacement.data[0].id },
            { query: { supervisorId: context.id } }
          );
      }
    ]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [
      async function(context) {
        await context.app.service("users").remove(context.result.userId);
      }
    ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
