const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const authRoleBoolean = require("../../hooks/role-based-auth-boolean");
const errors = require("@feathersjs/errors");
const {} = require("../../hooks/utility");
const { DateTime } = require("luxon");

async function setUserRoleId(context, key = "userId") {
  if (context.params.payload && context.params.payload.userId) {
    const role = context.params.payload.role;
    const user = await context.app.service(`${role}s`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    context.data[key] = user[0].id;
  }
}

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [],
    get: [],
    create: [
      async function(context) {
        //check roles
        const { method, query } = context.data;
        switch (method) {
          case "get_bookable_plots":
            authRole({
              super_admin: true,
              admin: true,
              dispatcher: true,
              technician: true
            })(context);
            if (authRoleBoolean({ technician: true })(context)) {
              await setUserRoleId(context, "technicianId");
            }
            break;

          case "get_available_dates":
            authRole({ super_admin: true, admin: true, technician: true })(
              context
            );
            query.startDate = DateTime.fromJSDate(new Date(query.startDate))
              .startOf("day")
              .toJSDate();
            query.endDate = DateTime.fromJSDate(new Date(query.endDate))
              .endOf("day")
              .toJSDate();

            if (
              authRoleBoolean({ technician: true })(context) ||
              !query.fecId
            ) {
              query.fecId = await context.app.service(`technicians`).find({
                paginate: false,
                query: { userId: context.params.payload.userId }
              });
              query.fecId = query.fecId[0].supervisor.fecId;
            }
            break;
          case "get_all_available_equipment":
            authRole({ super_admin: true, admin: true, dispatcher: true })(
              context
            );
            if (authRoleBoolean({ dispatcher: true })(context)) {
              query.fecId = await context.app.service(`dispatchers`).find({
                paginate: false,
                query: { userId: context.params.payload.userId }
              });
              query.fecId = query.fecId[0].fecId;
            }
            //client will send from 8 - 9, but we need to store as 8 - 8:59:59
            query.endDate = DateTime.fromJSDate(new Date(query.endDate))
              .minus({ hour: 1 })
              .endOf("hour")
              .toJSDate();
            break;
          case "create_booking":
            authRole({ super_admin: true, admin: true, technician: true })(
              context
            );
            //TODO check if has avaialble dates
            if (!query.fecId) {
              query.fecId = await context.app.service(`technicians`).find({
                paginate: false,
                query: { userId: context.params.payload.userId }
              });
              query.fecId = query.fecId[0].supervisor.fecId;
            }

            if (authRoleBoolean({ technician: true })(context)) {
              await setUserRoleId(context, "technicianId");
            }
            //client will send from 8 - 9, but we need to store as 8 - 8:59:59
            query.endDate = DateTime.fromJSDate(new Date(query.endDate))
              .minus({ hour: 1 })
              .endOf("hour")
              .toJSDate();
            break;
          case "create_booking_manual":
            authRole({ super_admin: true, admin: true, dispatcher: true })(
              context
            );
            if (authRoleBoolean({ dispatcher: true })(context)) {
              query.fecId = await context.app.service(`dispatchers`).find({
                paginate: false,
                query: { userId: context.params.payload.userId }
              });
              query.fecId = query.fecId[0].fecId;
            }
            //client will send from 8 - 9, but we need to store as 8 - 8:59:59
            query.endDate = DateTime.fromJSDate(new Date(query.endDate))
              .minus({ hour: 1 })
              .endOf("hour")
              .toJSDate();
            context.data.technicianId = query.technicianId
              ? query.technicianId
              : null;
            break;
          case "edit_booking_asset":
            authRole({ super_admin: true, admin: true, dispatcher: true })(
              context
            );
            //need check if asset belongs to same fec as dispatcher

            break;
          case "get_available_assets":
            authRole({ super_admin: true, admin: true, dispatcher: true })(
              context
            );
            if (authRoleBoolean({ dispatcher: true })(context)) {
              query.fecId = await context.app.service(`dispatchers`).find({
                paginate: false,
                query: { userId: context.params.payload.userId }
              });
              query.fecId = query.fecId[0].fecId;
            }
            break;
          default:
            throw new errors.BadRequest("invalid method");
        }

        return context;
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async function(context) {
        const { method, query } = context.data;
        let booking, participants;
        switch (method) {
          case "create_booking_manual":
          case "create_booking":
            //need to create data timestamp
            //get entire booking with all the linked data structure such as technicians etc.
            booking = await context.app
              .service("bookings")
              .get(context.result.id);
            participants = booking.farm_plot.formal_cluster
              ? [
                  await context.app
                    .service("formal-clusters")
                    .get(booking.farm_plot.formal_cluster.id)
                ]
              : await context.app.service("participants").find({
                  paginate: false,
                  query: booking.farm_plot.clusterId
                    ? {
                        clusterId: booking.farm_plot.clusterId
                      }
                    : { id: booking.farm_plot.participant.id }
                });
            context.result = await context.app
              .service("bookings")
              .patch(booking.id, { dataSnapshot: { booking, participants } });
            break;
          case "edit_booking_asset":
            booking = context.result;
            context.result = await context.app
              .service("bookings")
              .patch(booking.id, {
                dataSnapshot: Object.assign({}, booking.dataSnapshot, {
                  booking: Object.assign({}, booking.dataSnapshot.booking, {
                    equipment_assets: booking.equipment_assets
                  })
                })
              });
            break;
          default:
            break;
        }
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
