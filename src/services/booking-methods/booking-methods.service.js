// Initializes the `booking-methods` service on path `/booking-methods`
const createService = require("./booking-methods.class.js");
const hooks = require("./booking-methods.hooks");

module.exports = function(app) {
  const paginate = app.get("paginate");

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/booking-methods", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("booking-methods");

  service.hooks(hooks);
};
