const { DateTime } = require("luxon");
const errors = require("@feathersjs/errors");
const START_HOUR = 6; // 6 am
const END_HOUR = 18; // 6 pm
const START_END_HOURS = END_HOUR - START_HOUR;

/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }

  async create(data, params) {
    const { method, query } = data;
    const app = this.app;

    let result,
      service,
      assets,
      bookings,
      plots,
      participants,
      clusters,
      maintenance,
      equipment,
      equipmentAssets;

    switch (method) {
      case "get_bookable_plots":
        result = {};
        //get all plots under this technician
        plots = await app.service("farm-plots").find({
          paginate: false,
          query: Object.assign(
            {},
            data.technicianId ? { technicianId: data.technicianId } : {}
          )
        });

        //gather formal cluster names
        clusters = await app.service("formal-clusters").find({
          paginate: false,
          query: {
            id: {
              $in: [
                ...new Set(
                  plots
                    .filter(plot => plot.formalClusterId)
                    .map(plot => plot.formalClusterId)
                )
              ]
            }
          }
        });

        // need to figure if the plot is part of a informal cluster
        // gather participant ids for informal cluster and individual participant
        participants = await app.service("participants").find({
          paginate: false,
          query: {
            id: {
              $in: [
                ...new Set(
                  plots
                    .filter(plot => !plot.formalClusterId)
                    .map(plot => plot.participantId)
                )
              ]
            }
          }
        });

        // sort the plots into their participant / formal cluster ids.
        const participantPlots = {};
        const clusterPlots = {};
        const informalClusterPlots = {};
        const informalClusterParticipantPlots = {};
        plots.map(plot => {
          if (plot.formalClusterId) {
            if (clusterPlots[plot.formalClusterId]) {
              clusterPlots[plot.formalClusterId].push(plot);
            } else {
              clusterPlots[plot.formalClusterId] = [plot];
            }
          } else if (plot.clusterId) {
            //this is informal cluster
            if (informalClusterPlots[plot.clusterId]) {
              informalClusterPlots[plot.clusterId].push(plot);
            } else {
              informalClusterPlots[plot.clusterId] = [plot];
            }
            if (informalClusterParticipantPlots[plot.participantId]) {
              informalClusterParticipantPlots[plot.participantId].push(plot);
            } else {
              informalClusterParticipantPlots[plot.participantId] = [plot];
            }
          } else {
            if (participantPlots[plot.participantId]) {
              participantPlots[plot.participantId].push(plot);
            } else {
              participantPlots[plot.participantId] = [plot];
            }
          }
        });

        const informalClusterWithLeader = {};
        participants.map(participant => {
          //remove formal cluster participant (only tracking socio info)
          if (participant.cluster && participant.cluster.formal_cluster) return;
          if (participantPlots[participant.id]) {
            // an individual farmer
            result["participant-" + participant.id] = {
              participant,
              plots: participantPlots[participant.id]
            };

            if (!informalClusterParticipantPlots[participant.id]) {
              //participant dont have any plots that is part of informal cluster
              return;
            }
          }
          // from here all participants are informal participants i.e. part of informal cluster
          const informalPlots = informalClusterParticipantPlots[participant.id];
          informalPlots.map(plot => {
            const leader =
              !!participant.clusterLeaderId &&
              participant.clusterLeaderId === plot.clusterId;
            const clusterId = plot.clusterId;
            // part of an informal cluster
            let informalCluster = result["informal_cluster-" + clusterId];
            if (!informalCluster) {
              //does not exist in results need create first
              result["informal_cluster-" + clusterId] = {
                participants: [],
                plots: informalClusterPlots[clusterId]
              };
              informalCluster = result["informal_cluster-" + clusterId];

              informalClusterWithLeader[
                "informal_cluster-" + clusterId
              ] = false;
            }

            if (leader) {
              informalCluster["leader"] = participant;
              informalClusterWithLeader[
                "informal_cluster-" + participant.clusterLeaderId
              ] = true;
            } else {
              informalCluster["participants"].push(participant);
            }
          });
        });

        clusters.map(cluster => {
          result["cluster-" + cluster.id] = {
            cluster,
            plots: clusterPlots[cluster.id]
          };
        });

        // remove informal clusters with no leaders
        Object.keys(informalClusterWithLeader).map(key => {
          if (!informalClusterWithLeader[key]) {
            delete result[key];
          }
        });
        break;
      case "get_available_dates":
        // startDate, endDate, serviceId, fecId
        service = await app.service("services").get(query.serviceId);
        equipment = {};
        //get all equipments that are required to perform service
        service.equipment.map(equip => {
          equipment[equip.id] = 0;
        });
        //get all assets that exists in the fec.
        assets = await app.service("equipment-assets").find({
          paginate: false,
          query: {
            equipmentId: { $in: Object.keys(equipment) },
            fecId: query.fecId,
            condition: "working"
          }
        });

        //get total number of assets for each equipment
        assets.map(asset => {
          const equip = equipment[asset.equipmentId];
          equipment[asset.equipmentId] = equip + 1;
        });

        //handle corner case where there are no assets at all
        //we have to return that all dates are not available
        let cornerCaseNoAsset = false;
        Object.values(equipment).map(val => {
          if (val === 0) {
            cornerCaseNoAsset = true;
          }
        });
        if (cornerCaseNoAsset) {
          result = {};
          const sDate = DateTime.fromJSDate(query.startDate);
          const eDate = DateTime.fromJSDate(query.endDate);
          const interval = sDate.until(eDate);
          const noOfDays = interval.count("day");
          for (let day = 0; day < noOfDays; day++) {
            const currentDate = sDate
              .plus({ days: day })
              .toFormat("yyyy-MM-dd");
            result[currentDate] = false;
          }
          result = { datesUnavailable: result, hoursUnavailable: {} };
          break;
        }

        //get all bookings for the assets already made in the period
        bookings = await app.service("bookings").find({
          paginate: false,
          query: {
            status: "pending",
            "equipment-assets": {
              serial: { $in: assets.map(({ serial }) => serial) }
            },
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } }
            ]
          }
        });
        const bookedDates = {};
        bookings.map(booking => {
          const { startDate, endDate } = booking;
          const sDate = DateTime.fromJSDate(startDate);
          const eDate = DateTime.fromJSDate(endDate)
            .plus({ hour: 1 })
            .startOf("hour"); // as this will be H:59:59, we plus one so we can check the in betweens
          const interval = sDate.until(eDate);
          const noOfDays = interval.count("day");
          for (let day = 0; day < noOfDays; day++) {
            // for each day we only need the hours from start hours to end hours
            const currentDate = sDate.plus({ days: day });
            const startHour =
              day === 0 && currentDate.hour > START_HOUR
                ? currentDate.hour
                : START_HOUR;
            const endHour =
              day === noOfDays - 1 && eDate.hour < END_HOUR
                ? eDate.hour
                : END_HOUR;
            for (let hour = startHour; hour < endHour; hour++) {
              const currentDateHour = currentDate.set({ hour });
              const dateKey = currentDateHour.toFormat("yyyy-MM-dd H:mm");
              if (!bookedDates[dateKey]) {
                bookedDates[dateKey] = {};
              }
              booking["equipment_assets"].map(({ equipmentId }) => {
                if (bookedDates[dateKey][equipmentId]) {
                  bookedDates[dateKey][equipmentId] =
                    bookedDates[dateKey][equipmentId] + 1;
                } else {
                  bookedDates[dateKey][equipmentId] = 1;
                }
              });
            }
          }
        });

        //get all maintenance for the assets
        const maintenanceDates = {};
        maintenance = await app.service("equipment-asset-maintenance").find({
          paginate: false,
          query: {
            equipmentAssetSerial: { $in: assets.map(({ serial }) => serial) },
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } }
            ]
          }
        });
        maintenance.map(obj => {
          const { startDate, endDate } = obj;
          const sDate = DateTime.fromJSDate(startDate);
          const eDate = DateTime.fromJSDate(endDate);
          const interval = sDate.until(eDate);
          const noOfDays = interval.count("day");
          for (let day = 0; day < noOfDays; day++) {
            const currentDate = sDate.plus({ days: day });

            for (let hour = START_HOUR; hour < END_HOUR; hour++) {
              const currentDateHour = currentDate.set({ hour });
              const dateKey = currentDateHour.toFormat("yyyy-MM-dd H:mm");
              if (!bookedDates[dateKey]) {
                bookedDates[dateKey] = {};
              }
              if (!maintenanceDates[dateKey]) {
                maintenanceDates[dateKey] = {};
              }
              const equipmentId = obj.equipment_asset.equipmentId;
              if (!maintenanceDates[dateKey][obj.equipmentAssetSerial]) {
                //prevent for overlapping schedules to conflict the assets
                maintenanceDates[dateKey][obj.equipmentAssetSerial] = true;
                if (bookedDates[dateKey][equipmentId]) {
                  bookedDates[dateKey][equipmentId] =
                    bookedDates[dateKey][equipmentId] + 1;
                } else {
                  bookedDates[dateKey][equipmentId] = 1;
                }
              }
            }
          }
        });

        const resultHour = {};
        const hoursUnavailable = {};
        Object.keys(bookedDates).map(dateHour => {
          const bookedEquip = bookedDates[dateHour];
          Object.keys(equipment).map(equipId => {
            const maxAsset = equipment[equipId];
            const bookedAsset = bookedEquip[equipId];
            if (bookedAsset >= maxAsset) {
              const [date] = dateHour.split(" ");
              resultHour[date] = resultHour[date] ? resultHour[date] + 1 : 1;
              hoursUnavailable[dateHour] = false;
            }
          });
        });

        const datesUnavailable = {};
        Object.keys(resultHour).map(date => {
          if (resultHour[date] >= START_END_HOURS) {
            datesUnavailable[date] = false;
          }
        });
        result = { datesUnavailable, hoursUnavailable };
        // resuilt here will bascially contain the dates that are unavailable and hours
        break;

      case "get_all_available_equipment":
        equipment = {};
        //get all assets that exist in the fec
        assets = await app.service("equipment-assets").find({
          paginate: false,
          query: {
            fecId: query.fecId,
            condition: "working"
          }
        });
        //sum assets and the types of equipments
        assets.map(asset => {
          if (equipment[asset.equipmentId]) {
            const equip = equipment[asset.equipmentId];
            equipment[asset.equipmentId] = equip + 1;
          } else {
            equipment[asset.equipmentId] = 1;
          }
        });
        //get all bookings for the assets already made in the period
        bookings = await app.service("bookings").find({
          paginate: false,
          query: {
            status: "pending",
            "equipment-assets": {
              serial: { $in: assets.map(({ serial }) => serial) }
            },
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } }
            ]
          }
        });
        bookings.map(booking => {
          booking["equipment_assets"].map(({ equipmentId }) => {
            if (equipment[equipmentId]) {
              equipment[equipmentId] = equipment[equipmentId] - 1;
            }
          });
        });
        maintenance = await app.service("equipment-asset-maintenance").find({
          paginate: false,
          query: {
            equipmentAssetSerial: { $in: assets.map(({ serial }) => serial) },
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } }
            ]
          }
        });
        maintenance.map(obj => {
          const equipmentId = obj.equipment_asset.equipmentId;
          if (equipment[equipmentId]) {
            equipment[equipmentId] = equipment[equipmentId] - 1;
          }
        });
        result = { equipment };
        break;

      case "create_booking":
      case "create_booking_manual":
        //startDate, endDate, serviceId, fecId, plotId or clusterId
        if (query.clusterId) {
          //this is for informal cluster, create seperate POs, invoices etc. But for booking we will just get the cluster leader's plot
          participants = await app.service("participants").find({
            paginate: false,
            query: {
              clusterLeaderId: query.clusterId,
              clusterId: query.clusterId
            }
          });
          plots = participants[0].farm_plots[0].id;
        } else if (query.plotId) {
          plots = query.plotId;
        }

        service = await app.service("services").get(query.serviceId);
        //get all assets to perform this particular service
        equipmentAssets = {};
        assets = await app.service("equipment-assets").find({
          paginate: false,
          query: {
            equipmentId: {
              $in:
                method === "create_booking_manual"
                  ? //if its manual we take the equipment sent by the user
                    query.equipment.map(id => {
                      equipmentAssets[id] = { assets: [] };
                      return id;
                    })
                  : service.equipment.map(equip => {
                      equipmentAssets[equip.id] = { assets: [] };
                      return equip.id;
                    })
            },
            fecId: query.fecId,
            condition: "working"
          }
        });
        //sort assets into equipments

        assets.map(asset => {
          const id = asset.equipmentId;
          equipmentAssets[id].assets.push(asset.serial);
        });

        //get all assets that are already booked within that period
        bookings = await app.service("bookings").find({
          paginate: false,
          query: {
            status: "pending",
            "equipment-assets": {
              serial: { $in: assets.map(({ serial }) => serial) }
            },
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } },
              {
                $and: [
                  { startDate: { $lte: query.startDate } },
                  { endDate: { $gte: query.endDate } }
                ]
              }
            ]
          }
        });
        //remove from the pool of available assets

        bookings.map(booking => {
          const { equipment_assets } = booking;
          equipment_assets.map(asset => {
            const { serial, equipmentId } = asset;
            const arr = equipmentAssets[equipmentId].assets;
            if (arr.indexOf(serial) >= 0) {
              arr.splice(arr.indexOf(serial), 1);
            }
          });
        });

        //get all maintenance for the assets
        maintenance = await app.service("equipment-asset-maintenance").find({
          paginate: false,
          query: {
            equipmentAssetSerial: { $in: assets.map(({ serial }) => serial) },
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } }
            ]
          }
        });
        maintenance.map(obj => {
          const equipmentId = obj.equipment_asset.equipmentId;
          const serial = obj.equipmentAssetSerial;
          const arr = equipmentAssets[equipmentId].assets;
          if (arr.indexOf(serial) >= 0) {
            arr.splice(arr.indexOf(serial), 1);
          }
        });

        bookings = await app.service("bookings").create({
          startDate: query.startDate,
          endDate: query.endDate,
          status: "pending",
          farmPlotId: plots,
          serviceId: query.serviceId,
          technicianId: data.technicianId,
          discount: query.discount,
          individualInformal: !!query.individualInformal,
          additionalCosts: query.additionalCosts
            ? query.additionalCosts.map(obj => {
                let amount = 0;
                try {
                  amount = parseFloat(obj.amount);
                } catch (err) {}
                return { amount, type: obj.type };
              })
            : [],
          serviceArea: query.clusterId ? 0 : query.serviceArea, //if this is for informal cluster we will assume everything is being serviced.
          dataSnapshot: null
        });

        try {
          await app.service("booking-equipment-assets").create(
            Object.keys(equipmentAssets).map(equipmentId => {
              const equipAssets = equipmentAssets[equipmentId].assets;
              if (equipAssets.length === 0) {
                //not enough assets
                throw new errors.Conflict("assets not available");
              }
              return {
                equipmentAssetSerial: equipAssets[0],
                bookingId: bookings.id
              };
            })
          );
        } catch (err) {
          await app.service("bookings").remove(bookings.id);
          throw new errors.Conflict("assets not available");
        }

        result = await app.service("bookings").get(bookings.id);
        break;

      case "get_available_assets":
        assets = await app.service("equipment-assets").find({
          paginate: false,
          query: {
            equipmentId: { $in: query.equipmentIds },
            fecId: query.fecId,
            condition: "working"
          }
        });
        equipment = {};
        equipmentAssets = {};
        assets.map(({ serial, equipmentId }) => {
          equipmentAssets[serial] = true;
          if (equipment[equipmentId]) {
            equipment[equipmentId].push(serial);
          } else {
            equipment[equipmentId] = [serial];
          }
        });

        //get all assets fo that are already booked within that period
        bookings = await app.service("bookings").find({
          paginate: false,
          query: {
            status: "pending",
            "equipment-assets": {
              equipmentId: { $in: query.equipmentIds }
            },
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } },
              {
                $and: [
                  { startDate: { $lte: query.startDate } },
                  { endDate: { $gte: query.endDate } }
                ]
              }
            ]
          }
        });
        bookings.map(({ equipment_assets }) => {
          equipment_assets.map(({ serial }) => {
            delete equipmentAssets[serial];
          });
        });
        result = {};
        query.equipmentIds.map(id => {
          result[id] = [];
        });
        Object.keys(equipment).map(equipmentId => {
          const assets = equipment[equipmentId];
          assets.map(serial => {
            if (equipmentAssets[serial]) {
              //asset is available;
              result[equipmentId].push(serial);
            }
          });
        });
        break;
      case "edit_booking_asset":
        await app
          .service("booking-equipment-assets")
          .remove(null, { query: { bookingId: query.bookingId } });
        await app.service("booking-equipment-assets").create(
          query.assets.map(serial => ({
            equipmentAssetSerial: serial,
            bookingId: query.bookingId
          }))
        );
        result = await app.service("bookings").get(query.bookingId);
        break;
    }

    return result;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
