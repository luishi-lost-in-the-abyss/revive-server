// Initializes the `location-province` service on path `/location-province`
const createService = require("feathers-sequelize");
const createModel = require("../../models/location-province.model");
const hooks = require("./location-province.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/location-province", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("location-province");

  service.hooks(hooks);
};
