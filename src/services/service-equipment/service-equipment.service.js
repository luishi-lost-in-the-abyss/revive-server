// Initializes the `service-equipment` service on path `/service-equipment`
const createService = require("feathers-sequelize");
const createModel = require("../../models/service-equipment.model");
const hooks = require("./service-equipment.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/service-equipment", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("service-equipment");

  service.hooks(hooks);
};
