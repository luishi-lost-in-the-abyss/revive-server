const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");

const adminOnly = authRole({ super_admin: true, admin: true });

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        dispatcher: true,
        technician: true
      })
    ],
    find: [],
    get: [],
    create: [adminOnly],
    update: [disallow("external")],
    patch: [adminOnly],
    remove: [adminOnly]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
