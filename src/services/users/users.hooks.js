const { authenticate } = require("@feathersjs/authentication").hooks;
const { disallow, isProvider } = require("feathers-hooks-common");
const errors = require("@feathersjs/errors");
const { validateFarmPlots } = require("../../hooks/utility");

const {
  hashPassword,
  protect
} = require("@feathersjs/authentication-local").hooks;

function restrictOwnUser(context) {
  //only allow user to modify/get his own user id
  if (context.params.payload && context.params.payload.userId) {
    //if from server this will not be invoked.
    context.id = context.params.payload.userId;
  }
  return context;
}

module.exports = {
  before: {
    all: [],
    find: [disallow("external")],
    get: [authenticate("jwt"), restrictOwnUser],
    create: [
      hashPassword(),
      async function(context) {
        if (isProvider("server")(context)) return context;
        //force participant role, admin will use admin create method to bypass this
        context.data.role = "participant";
        context.data.status = "pending";
        if (context.params.payload && context.params.payload.userId) {
          const technician = await context.app
            .service("technicians")
            .find({ query: { userId: context.params.payload.userId } });
          context.data.createdById = technician.data[0].id;
        }

        //validation of participant details
        const { plots } = context.data;
        if (plots.length === 0) throw new errors.BadRequest("no farm plots");
        validateFarmPlots(plots);

        return context;
      }
    ],
    update: [hashPassword(), authenticate("jwt")],
    patch: [
      hashPassword(),
      authenticate("jwt"),
      async function(context) {
        if (isProvider("server")(context)) return context;
        // only allow patching of own id
        context.id = context.params.payload.userId;
      }
    ],
    remove: [
      authenticate("jwt"),
      async function(context) {
        const user = await context.app.service("users").get(context.id);
        if (user.role === "participant" && user.status === "pending") {
          //rejection of application delete all farmplots and participant
          let participant = await context.app
            .service("participants")
            .find({ query: { userId: context.id } });
          await context.app
            .service("farm-plots")
            .remove(null, { query: { participantId: participant.data[0].id } });
          await context.app
            .service("participants")
            .remove(participant.data[0].id);
        }
      }
    ]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect("password")
    ],
    find: [],
    get: [],
    create: [
      async function(context) {
        const { id: userId, role } = context.result;
        const {
          firstName,
          middleName,
          lastName,
          birthdate,
          occupation,
          civil,
          contact,
          operator,
          gender,
          residentAddress,
          bankName,
          bankAccType,
          bankAccName,
          bankTinNo,
          healthConditions,
          healthOthers,
          socio,
          beneficiary,
          plots,
          cluster,
          file,
          createdById
        } = context.data;
        if (role === "participant") {
          //create participant profile
          const participantProfile = await context.app
            .service("participants")
            .create({
              file,
              userId,
              formalClusterId: cluster ? cluster : null,
              firstName,
              middleName,
              lastName,
              birthdate,
              occupation,
              civil,
              contact,
              operator,
              gender,
              residentAddress,
              bankName,
              bankAccType,
              bankAccName,
              bankTinNo,
              healthConditions,
              healthOthers,
              createdById: createdById ? createdById : null,
              farm_plots: plots.map(
                ({
                  cropInsurance,
                  barangay,
                  commodity,
                  commodityOthers,
                  clusterId,
                  ...plot
                }) => {
                  plot.cropInsurance = cropInsurance
                    ? plot.cropInsuranceDetails
                    : null;
                  plot.barangayId = barangay;
                  plot.commodity =
                    commodity === "others"
                      ? !!commodityOthers
                        ? commodityOthers
                        : "others"
                      : commodity;
                  plot.clusterId = clusterId ? clusterId : null;
                  return plot;
                }
              )
            });
          const socioInfo = await context.app.service("socio-info").create({
            data: Object.assign(socio, { beneficiary }),
            participantId: participantProfile.id
          });
          context.result.profile = participantProfile;
        }
        return context;
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
