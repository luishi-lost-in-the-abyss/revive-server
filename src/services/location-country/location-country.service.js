// Initializes the `location-country` service on path `/location-country`
const createService = require("feathers-sequelize");
const createModel = require("../../models/location-country.model");
const hooks = require("./location-country.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/location-country", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("location-country");

  service.hooks(hooks);
};
