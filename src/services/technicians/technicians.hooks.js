const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const authRoleBool = require("../../hooks/role-based-auth-boolean");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const disablePagination = require("../../hooks/disablePagination");
const locationJoin = require("../../hooks/locationJoin");

const adminOnly = authRole({ super_admin: true, admin: true });

const innerJoinFunction = function(context) {
  const Users = context.app.services["users"].Model;
  const Supervisors = context.app.services["supervisors"].Model;
  const Fec = context.app.services["fec"].Model;

  const supervisorModel = {
    model: Supervisors,
    attributes: ["id", "name", "fecId"],
    include: [{ model: Fec }]
  };
  if (context.params.query.fecId) {
    supervisorModel.where = { fecId: context.params.query.fecId };
    delete context.params.query.fecId;
  }

  context.params.sequelize = {
    raw: false,
    include: [
      {
        model: Users,
        attributes: ["email", "username"]
      },
      supervisorModel,
      locationJoin(context)
    ]
  };
  return context;
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        technician: true,
        dispatcher: true,
        supervisor: true
      })
    ],
    find: [
      disablePagination,
      innerJoinFunction,
      async function(context) {
        if (authRoleBool({ supervisor: true })(context)) {
          const supervisor = await context.app
            .service("supervisors")
            .find({ query: { userId: context.params.payload.userId } });
          context.params.query = Object.assign({}, context.params.query, {
            supervisorId: supervisor.data[0].id
          });
        }
      }
    ],
    get: [innerJoinFunction],
    create: [adminOnly],
    update: [disallow("external")],
    patch: [adminOnly],
    remove: [adminOnly]
  },

  after: {
    all: [],
    find: [],
    get: [
      async function(context) {
        if (authRoleBool({ supervisor: true })(context)) {
          const supervisor = await context.app
            .service("supervisors")
            .find({ query: { userId: context.params.payload.userId } });
          if (context.result.supervisorId !== supervisor.data[0].id) {
            throw new errors.NotFound();
          }
        }
      }
    ],
    create: [],
    update: [],
    patch: [],
    remove: [
      async function(context) {
        await context.app.service("users").remove(context.result.userId);
      }
    ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
