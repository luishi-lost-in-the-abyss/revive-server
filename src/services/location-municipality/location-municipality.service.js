// Initializes the `location-municipality` service on path `/location-municipality`
const createService = require("feathers-sequelize");
const createModel = require("../../models/location-municipality.model");
const hooks = require("./location-municipality.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/location-municipality", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("location-municipality");

  service.hooks(hooks);
};
