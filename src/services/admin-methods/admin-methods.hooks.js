const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");
const { validateFarmPlots } = require("../../hooks/utility");

const beforeCreateFormalCluster = async function(context) {
  const { username, plots, code } = context.data.query;
  validateFarmPlots(plots);
  //check if username exists
  const userExists = await context.app
    .service("users")
    .find({ query: { username } });
  if (userExists.total !== 0) throw new errors.Conflict("username exists");
  const clusterCodeExists = await context.app
    .service("clusters")
    .find({ query: { code } });
  if (clusterCodeExists.total !== 0)
    throw new errors.Conflict("cluster exists");
};

const beforeEditFormalCluster = async function(context) {
  const { formalCluster, code, id } = context.data.query;
  validateFarmPlots(formalCluster.plots);
  const clusterCodeExists = await context.app
    .service("clusters")
    .find({ query: { code } });
  if (clusterCodeExists.total !== 0 && clusterCodeExists.data[0].id !== id)
    throw new errors.Conflict("cluster exists");
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      async function(context) {
        switch (context.data.method) {
          case "create_formal_cluster":
          case "edit_formal_cluster":
            authRole({ admin: true, super_admin: true, supervisor: true })(
              context
            );
            break;

          default:
            authRole({ admin: true, super_admin: true })(context);
        }
      }
    ],
    find: [],
    get: [],
    create: [
      async function(context) {
        const data = context.data;
        const { method, query = {} } = data;
        let user, participant, plots, jsonObj;
        switch (method) {
          case "get_pending_applications":
            break;
          case "approve_application":
            // check if all plots have a technician
            jsonObj = {};
            query.plotTechnicians.map(({ plotId }) => {
              jsonObj[plotId] = true;
            });
            participant = await context.app
              .service("participants")
              .find({ query: { userId: query.userId } });
            if (participant.total !== 1)
              throw new errors.BadRequest("no such application");
            participant.data[0]["farm_plots"].map(plot => {
              if (!jsonObj[plot.id])
                throw new errors.BadRequest("missing plot assignment");
            });
          case "reject_application":
            //check to see if application is pending
            user = await context.app.service("users").get(query.userId);
            if (
              !user ||
              user.role !== "participant" ||
              user.status !== "pending"
            )
              throw new errors.BadRequest("no such application");

            break;
          case "create_formal_cluster":
            beforeCreateFormalCluster(context);
            break;
          case "edit_formal_cluster":
            beforeEditFormalCluster(context);
            break;

          case "create_supervisor":
          case "edit_supervisor":
          case "create_dispatcher":
          case "edit_dispatcher":
          case "create_technician":
          case "edit_technician":
            break;

          default:
            throw new errors.BadRequest("invalid method");
        }
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
