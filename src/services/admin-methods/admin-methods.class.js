/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }

  async create(data, params) {
    const { method, query = {} } = data;
    const app = this.app;
    let results, users, participants, cluster, farmPlots, promises;

    switch (method) {
      case "get_pending_applications":
        results = await app.service("participants").find({
          query: { userRole: "participant", userStatus: "pending" }
        });
        break;

      case "approve_application":
        results = await app
          .service("users")
          .patch(query.userId, { status: "activated" });
        query.plotTechnicians.map(({ plotId, technicianId }) => {
          app.service("farm-plots").patch(plotId, { technicianId });
        });
        break;

      case "reject_application":
        results = await app.service("users").remove(query.userId);
        break;

      case "create_formal_cluster":
        users = await app.service("users").create({
          username: query.username,
          password: query.password,
          email: query.email,
          status: "activated",
          role: "cluster-participant"
        });
        cluster = await app.service("clusters").create({ code: query.code });
        cluster = await app.service("formal-clusters").create(
          Object.assign(
            {
              userId: users.id,
              clusterId: cluster.id,
              farm_plots: query.plots.map(({ barangay, ...plot }, i) => {
                plot.clusterId = null;
                plot.barangayId = barangay;
                plot.technicianId = query.inputTechnician[i];
                plot.cropInsurance = plot.cropInsurance
                  ? plot.cropInsuranceDetails
                  : null;
                return plot;
              })
            },
            query
          )
        );
        results = cluster;
        break;

      case "edit_formal_cluster":
        // edit cluster info
        cluster = await app
          .service("clusters")
          .patch(query.id, { code: query.code });
        results = Object.assign({}, cluster.dataValues);

        // edit formal cluster info
        cluster = await app
          .service("formal-clusters")
          .find({ query: { clusterId: query.id } });
        cluster = await app
          .service("formal-clusters")
          .patch(cluster.data[0].id, query.formalCluster);
        results = Object.assign(results, { formal_cluster: cluster });

        // edit cluster farm plots
        farmPlots = await app.service("farm-plots").find({
          paginate: false,
          query: { formalClusterId: results.formal_cluster.id }
        });
        cluster = {};
        query.formalCluster.plots.map(({ barangay, ...plot }, i) => {
          cluster[plot.id] = plot;
          cluster[plot.id].clusterId = null;
          cluster[plot.id].barangayId = barangay;
          cluster[plot.id].technicianId =
            query.formalCluster.inputTechnician[i];
          cluster[plot.id].cropInsurance = plot.cropInsurance
            ? plot.cropInsuranceDetails
            : null;
        });
        promises = [];
        farmPlots.map(plot => {
          if (cluster[plot.id]) {
            //exists so we edit the plot
            promises.push(
              app.service("farm-plots").patch(plot.id, cluster[plot.id])
            );
            delete cluster[plot.id];
          } else {
            // we are supposed to delete it
            promises.push(app.service("farm-plots").remove(plot.id));
          }
        });
        //remainder of clusters are supposed to be added
        Object.keys(cluster).map(id => {
          const clusterObj = cluster[id];
          promises.push(
            app.service("farm-plots").create(
              Object.assign({}, clusterObj, {
                formalClusterId: results.formal_cluster.id
              })
            )
          );
        });

        await Promise.all(promises);
        break;

      case "create_supervisor":
        users = await app.service("users").create({
          username: query.username,
          email: query.email,
          password: query.password,
          role: "supervisor",
          status: "activated"
        });
        results = await app.service("supervisors").create({
          name: query.name,
          fecId: query.fec,
          contact: query.contact,
          userId: users.id
        });
        results = Object.assign({}, results, { user: users });
        break;

      case "edit_supervisor":
        results = await app.service("supervisors").patch(query.supervisorId, {
          name: query.name,
          fecId: query.fec,
          contact: query.contact
        });

        users = await app.service("users").patch(
          results.userId,
          Object.assign(
            {
              email: query.email
            },
            query.password
              ? {
                  password: query.password
                }
              : {}
          )
        );
        results = Object.assign({}, results, { user: users });
        break;

      case "create_technician":
        users = await app.service("users").create({
          username: query.username,
          email: query.email,
          password: query.password,
          role: "technician",
          status: "activated"
        });
        results = await app.service("technicians").create({
          name: query.name,
          contact: query.contact,
          userId: users.id,
          supervisorId: query.supervisor,
          barangayId: query.barangayId
        });
        results = Object.assign({}, results, { user: users });
        break;

      case "edit_technician":
        results = await app.service("technicians").patch(query.technicianId, {
          name: query.name,
          contact: query.contact,
          supervisorId: query.supervisor,
          barangayId: query.barangayId
        });
        await app
          .service("users")
          .patch(
            results.userId,
            Object.assign(
              { email: query.email },
              query.password ? { password: query.password } : {}
            )
          );
        break;
      case "create_dispatcher":
        users = await app.service("users").create({
          username: query.username,
          email: query.email,
          password: query.password,
          role: "dispatcher",
          status: "activated"
        });
        results = await app.service("dispatchers").create({
          name: query.name,
          contact: query.contact,
          userId: users.id,
          fecId: query.fecId
        });
        results = Object.assign({}, results, { user: users });
        break;

      case "edit_dispatcher":
        results = await app.service("dispatchers").patch(query.dispatcherId, {
          name: query.name,
          contact: query.contact,
          fecId: query.fecId
        });

        users = await app.service("users").patch(
          results.userId,
          Object.assign(
            {
              email: query.email
            },
            query.password
              ? {
                  password: query.password
                }
              : {}
          )
        );
        results = Object.assign({}, results, { user: users });
        break;
    }

    return results;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
