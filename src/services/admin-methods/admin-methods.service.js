// Initializes the `admin-methods` service on path `/admin-methods`
const createService = require("./admin-methods.class.js");
const hooks = require("./admin-methods.hooks");

module.exports = function(app) {
  const paginate = app.get("paginate");

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/admin-methods", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("admin-methods");

  service.hooks(hooks);
};
