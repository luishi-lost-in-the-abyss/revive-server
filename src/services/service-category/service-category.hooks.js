const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const disablePagination = require("../../hooks/disablePagination");

const adminOnly = authRole({ super_admin: true, admin: true });

const innerJoinFunction = function(context) {
  const Services = context.app.services["services"].Model;
  const ServiceCategory = context.app.services["service-category"].Model;

  context.params.sequelize = {
    raw: false,
    include: [
      { model: Services },
      { model: ServiceCategory, as: "parent_category" }
    ]
  };
  return context;
};
module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        dispatcher: true,
        technician: true
      })
    ],
    find: [disablePagination],
    get: [innerJoinFunction],
    create: [
      adminOnly,
      async function(context) {
        if (!context.data.parentCategoryId) {
          context.data.parentCategoryId = null;
        }
      }
    ],
    update: [disallow("external")],
    patch: [
      adminOnly,
      async function(context) {
        if (!context.data.parentCategoryId) {
          context.data.parentCategoryId = null;
        }
      }
    ],
    remove: [adminOnly]
  },

  after: {
    all: [],
    find: [],
    get: [
      async function(context) {
        let serviceCategory = context.result;
        const parentCategories = [];
        if (serviceCategory) {
          parentCategories.push(serviceCategory.dataValues);
          while (serviceCategory.parentCategoryId) {
            serviceCategory = await context.app
              .service("service-category")
              .get(serviceCategory.parentCategoryId);
            parentCategories.push(serviceCategory);
          }
        }
        context.result = Object.assign({}, context.result.dataValues, {
          parent_category: parentCategories.reverse()
        });
        return context;
      }
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
