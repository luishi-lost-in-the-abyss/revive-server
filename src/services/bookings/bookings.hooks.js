const { authenticate } = require("@feathersjs/authentication").hooks;
const errors = require("@feathersjs/errors");
const authRoleBoolean = require("../../hooks/role-based-auth-boolean");
const locationJoin = require("../../hooks/locationJoin");
const Sequelize = require("sequelize");
const { DateTime } = require("luxon");

const innerJoinFunction = function(context) {
  const EquipmentAssets = context.app.services["equipment-assets"].Model;
  const FarmPlots = context.app.services["farm-plots"].Model;
  const Services = context.app.services["services"].Model;
  const Participants = context.app.services["participants"].Model;
  const FormalClusters = context.app.services["formal-clusters"].Model;
  const Clusters = context.app.services["clusters"].Model;
  const Equipment = context.app.services["equipment"].Model;
  const Technicians = context.app.services["technicians"].Model;
  const Users = context.app.services["users"].Model;
  const ServiceCategory = context.app.services["service-category"].Model;

  const query = context.params.query;

  const equipmentAssetsObj = {
    model: EquipmentAssets,
    include: [
      {
        model: Equipment,
        attributes: ["name"]
      }
    ]
  };
  if (query && query["equipment-assets"]) {
    equipmentAssetsObj.where = query["equipment-assets"];
    delete query["equipment-assets"];
  }

  if (query && query["fecId"]) {
    equipmentAssetsObj.where = Object.assign({}, equipmentAssetsObj.where, {
      fecId: query["fecId"]
    });
    delete query["fecId"];
  }

  const serviceObj = {
    model: Services,
    attributes: ["code", "name", "price", "serviceCategoryId"],
    include: [
      {
        model: ServiceCategory,
        include: [{ model: ServiceCategory, as: "parent_category" }]
      }
    ]
  };
  if (query && query["serviceCode"]) {
    serviceObj.where = { code: query["serviceCode"] };
    delete query["serviceCode"];
  }

  const participantNameFunc = Sequelize.fn(
    "concat",
    Sequelize.col("farm_plot->participant.firstName"),
    " ",
    Sequelize.col("farm_plot->participant.middleName"),
    " ",
    Sequelize.col("farm_plot->participant.lastName")
  );
  const participantObj = {
    model: Participants,
    attributes: [
      ...Object.keys(Participants.attributes).map(key => key),
      [participantNameFunc, "name"]
    ],
    as: "participant"
  };
  if (query && query["participantName"]) {
    participantObj.where = Sequelize.where(
      participantNameFunc,
      query["participantName"]
    );
    delete query["participantName"];
  }

  const formalClusterNameFunc = Sequelize.fn(
    "concat",
    Sequelize.col("farm_plot->formal_cluster.firstName"),
    " ",
    Sequelize.col("farm_plot->formal_cluster.middleName"),
    " ",
    Sequelize.col("farm_plot->formal_cluster.lastName")
  );
  const formalClusterObj = {
    model: FormalClusters,
    include: [{ model: Clusters, attributes: ["code"] }],
    attributes: [
      ...Object.keys(FormalClusters.attributes).map(key => key),
      [formalClusterNameFunc, "name"]
    ]
  };
  if (query && query["formalParticipantName"]) {
    formalClusterObj.where = Sequelize.where(
      formalClusterNameFunc,
      query["participantName"]
    );
    delete query["formalParticipantName"];
  }

  const technicianObj = {
    model: Technicians,
    attributes: ["name", "userId"],
    include: [{ model: Users, attributes: ["username"] }]
  };
  if (query && query["technicianName"]) {
    technicianObj.where = { name: query["technicianName"] };
    delete query["technicianName"];
  }

  context.params.sequelize = {
    raw: false,
    include: [
      {
        model: FarmPlots,
        required: true,
        include: [participantObj, formalClusterObj, locationJoin(context)]
      },
      serviceObj,
      equipmentAssetsObj,
      technicianObj
    ]
  };
  if (
    query &&
    (query["showOnlyType"] === "system" ||
      query["showOnlyType"] === "technician")
  ) {
    context.params.sequelize.where =
      query["showOnlyType"] === "system"
        ? { technicianId: null }
        : { technicianId: { $not: null } };
  }
  if (query) {
    delete query["showOnlyType"];
  }

  return context;
};

const restrictTechnician = async function(context) {
  //limit to own searches for technicians
  if (authRoleBoolean({ technician: true })(context)) {
    const technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    context.params.query = Object.assign({}, context.params.query, {
      technicianId: technician[0].id
    });
  }
};

const restrictSupervisor = async function(context) {
  //limit to own searches for technicians
  if (authRoleBoolean({ supervisor: true })(context)) {
    const supervisor = await context.app.service(`supervisors`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    const technicians = await context.app.service(`technicians`).find({
      paginate: false,
      query: { supervisorId: supervisor[0].id }
    });
    context.params.sequelize.include[0].where = {
      technicianId: { $in: technicians.map(({ id }) => id) }
    };
  }
};

const restrictDispatcherFec = async function(context) {
  //limit to own fec searches for dispatcher
  if (authRoleBoolean({ dispatcher: true })(context)) {
    const dispatcher = await context.app.service(`dispatchers`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    context.params.query = Object.assign({}, context.params.query, {
      fecId: dispatcher[0].fecId
    });
  }
};

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [
      restrictDispatcherFec,
      innerJoinFunction,
      restrictTechnician,
      restrictSupervisor
    ],
    get: [
      restrictDispatcherFec,
      innerJoinFunction,
      restrictTechnician,
      restrictSupervisor
    ],
    create: [innerJoinFunction],
    update: [],
    patch: [
      restrictDispatcherFec,
      async function(context) {
        const booking = await context.app.service("bookings").get(context.id, {
          query:
            context.params.query && context.params.query.fecId
              ? { fecId: context.params.query.fecId }
              : {}
        });
        if (context.params.query) {
          delete context.params.query.fecId;
        }
        if (context.data.status && context.data.status === "completed") {
          //check if date time is after, only can complete if after
          if (
            DateTime.fromJSDate(new Date()) <
            DateTime.fromJSDate(new Date(booking.startDate))
          ) {
            throw new errors.Conflict("before start date");
          }
        }
      }
    ],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
