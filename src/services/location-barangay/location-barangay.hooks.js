const disablePagination = require("../../hooks/disablePagination");
const locationJoin = require("../../hooks/locationJoin");
const innerJoinFunction = function(context) {
  context.params.sequelize = {
    raw: false,
    include: [...locationJoin(context).include] //the location join function returns with barangay model
  };
};
module.exports = {
  before: {
    all: [],
    find: [disablePagination, innerJoinFunction],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
