// Initializes the `location-barangay` service on path `/location-barangay`
const createService = require("feathers-sequelize");
const createModel = require("../../models/location-barangay.model");
const hooks = require("./location-barangay.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/location-barangay", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("location-barangay");

  service.hooks(hooks);
};
