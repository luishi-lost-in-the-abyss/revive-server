// Initializes the `supervisor-methods` service on path `/supervisor-methods`
const createService = require("./supervisor-methods.class.js");
const hooks = require("./supervisor-methods.hooks");

module.exports = function(app) {
  const paginate = app.get("paginate");

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/supervisor-methods", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("supervisor-methods");

  service.hooks(hooks);
};
