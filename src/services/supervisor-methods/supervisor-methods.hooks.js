const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");

module.exports = {
  before: {
    all: [authenticate("jwt"), authRole({ supervisor: true })],
    find: [],
    get: [],
    create: [
      async function(context) {
        const data = context.data;
        const { method, query = {} } = data;
        let supervisor, user, participant, plots, jsonObj;
        supervisor = await context.app
          .service("supervisors")
          .find({ query: { userId: context.params.payload.userId } });
        supervisor = supervisor.data[0].id;
        technicians = await context.app
          .service("technicians")
          .find({ query: { $limit: "-1", supervisorId: supervisor } });
        switch (method) {
          case "get_cluster_ids":
          case "get_pending_applications":
            context.data.supervisorId = supervisor;
            context.data.technicians = technicians;
            break;
          case "approve_application":
            // check if all plots have a technician
            jsonObj = {};
            query.plotTechnicians.map(({ plotId }) => {
              jsonObj[plotId] = true;
            });
            participant = await context.app.service("participants").find({
              query: {
                userId: query.userId,
                createdById: { $in: technicians.map(({ id }) => id) }
              }
            });
            if (participant.total !== 1)
              throw new errors.BadRequest("no such application");
            participant.data[0]["farm_plots"].map(plot => {
              if (!jsonObj[plot.id])
                throw new errors.BadRequest("missing plot assignment");
            });
            break;
          case "reject_application":
            //check to see if application is pending
            user = await context.app.service("users").get(query.userId);
            if (
              !user ||
              user.role !== "participant" ||
              user.status !== "pending"
            )
              throw new errors.BadRequest("no such application");

            break;

          default:
            throw new errors.BadRequest("invalid method");
        }
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
