/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }
  async create(data, params) {
    const { method, query = {} } = data;
    const app = this.app;
    let results, users, participants, cluster, farmPlots, promises, technicians;
    switch (method) {
      case "get_cluster_ids":
        results = await app.service("farm-plots").find({
          paginate: false,
          query: {
            technicianId: { $in: data.technicians.map(({ id }) => id) }
          }
        });
        results = [
          ...new Set(
            results
              .filter(({ clusterId }) => !!clusterId)
              .map(({ clusterId }) => clusterId)
          )
        ].concat(
          (await app.service("formal-clusters").find({
            paginate: false,
            query: {
              id: {
                $in: [
                  ...new Set(
                    results
                      .filter(({ formalClusterId }) => !!formalClusterId)
                      .map(({ formalClusterId }) => formalClusterId)
                  )
                ]
              }
            }
          })).map(({ clusterId }) => clusterId)
        );
        break;
      case "get_pending_applications":
        results = await app.service("participants").find({
          query: {
            userStatus: "pending",
            userRole: "participant",
            createdById: { $in: data.technicians.map(({ id }) => id) }
          }
        });
        break;

      case "approve_application":
        results = await app
          .service("users")
          .patch(query.userId, { status: "activated" });
        query.plotTechnicians.map(({ plotId, technicianId }) => {
          app.service("farm-plots").patch(plotId, { technicianId });
        });
        break;

      case "reject_application":
        results = await app.service("users").remove(query.userId);
        break;
    }

    return results;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
