const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const authRoleBool = require("../../hooks/role-based-auth-boolean");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const disablePagination = require("../../hooks/disablePagination");
const locationJoin = require("../../hooks/locationJoin");
const Sequelize = require("sequelize");

const adminOnly = authRole({ super_admin: true, admin: true });
const adminSupervisorOnly = authRole({
  super_admin: true,
  admin: true,
  supervisor: true
});

const innerJoinFunction = async function(context) {
  const Participants = context.app.services["participants"].Model;
  const Users = context.app.services["users"].Model;
  const FormalCluster = context.app.services["formal-clusters"].Model;
  const FarmPlots = context.app.services["farm-plots"].Model;
  const Technicians = context.app.services["technicians"].Model;

  const ParticipantsAttributes = [
    ...Object.keys(Participants.attributes).map(key => key),
    [
      Sequelize.fn(
        "concat",
        Sequelize.col("participants.firstName"),
        " ",
        Sequelize.col("participants.middleName"),
        " ",
        Sequelize.col("participants.lastName")
      ),
      "name"
    ]
  ];
  const LeaderAttributes = [
    ...Object.keys(Participants.attributes).map(key => key),
    [
      Sequelize.fn(
        "concat",
        Sequelize.col("Leader.firstName"),
        " ",
        Sequelize.col("Leader.middleName"),
        " ",
        Sequelize.col("Leader.lastName")
      ),
      "name"
    ]
  ];

  const FarmPlotsModel = {
    model: FarmPlots,
    include: [
      { model: Participants },
      {
        model: Technicians,
        attributes: ["id", "name", "barangayId"],
        include: [{ model: Users, attributes: ["username"] }]
      },
      locationJoin(context)
    ]
  };
  const FormalClusterFarmPlot = {
    model: FarmPlots,
    include: [
      {
        model: Technicians,
        attributes: ["id", "name", "barangayId"],
        include: [{ model: Users, attributes: ["username"] }]
      },
      locationJoin(context)
    ]
  };

  context.params.sequelize = {
    raw: false,
    include: [
      { model: Participants, attributes: LeaderAttributes, as: "Leader" },
      FarmPlotsModel,
      {
        model: FormalCluster,
        include: [FormalClusterFarmPlot, { model: Participants }],
        attributes: [
          ...Object.keys(FormalCluster.attributes).map(key => key),
          [
            Sequelize.fn(
              "concat",
              Sequelize.col("formal_cluster.firstName"),
              " ",
              Sequelize.col("formal_cluster.middleName"),
              " ",
              Sequelize.col("formal_cluster.lastName")
            ),
            "name"
          ]
        ]
      }
    ]
  };
  return context;
};

const disallowFormalClusters = async function(context) {
  //formal clusters must use admin methods to edit
  if (!(context.params || {}).provider) return context;
  const { formal } = context.data;
  if (typeof formal !== "undefined")
    throw new errors.BadRequest("use admin methods");
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        supervisor: true,
        technician: true
      })
    ],
    find: [disablePagination, innerJoinFunction],
    get: [innerJoinFunction],
    create: [adminSupervisorOnly, disallowFormalClusters],
    update: [disallow("external")],
    patch: [
      adminSupervisorOnly,
      innerJoinFunction,
      disallowFormalClusters,
      async function(context) {
        const { clusterLeaderId } = context.data;
        let participant;
        if (clusterLeaderId) {
          //check if clusterLeader has farm plots that is part of the cluster
          participant = await context.app
            .service("participants")
            .get(clusterLeaderId);
          let hasFarmPlot = false;
          participant.farm_plots.map(({ clusterId }) => {
            if (clusterId == context.id) {
              hasFarmPlot = true;
            }
          });
          if (!hasFarmPlot)
            throw new errors.BadRequest("cluster leader not part of cluster");
        }
        return context;
      }
    ],
    remove: [adminOnly]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [
      async function(context) {
        const { clusterLeaderId } = context.data;
        if (clusterLeaderId) {
          const prevClusterLeader = context.result.Leader;
          if (prevClusterLeader && prevClusterLeader.id !== clusterLeaderId) {
            // delete the previous cluster leader
            await context.app
              .service("participants")
              .patch(prevClusterLeader.id, { clusterLeaderId: null });
          }
          await context.app
            .service("participants")
            .patch(clusterLeaderId, { clusterLeaderId: context.id });
        }
      }
    ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
