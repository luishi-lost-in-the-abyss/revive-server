// Initializes the `clusters` service on path `/clusters`
const createService = require("feathers-sequelize");
const createModel = require("../../models/clusters.model");
const hooks = require("./clusters.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/clusters", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("clusters");

  service.hooks(hooks);
};
