// Initializes the `public-methods` service on path `/public-methods`
const createService = require("./public-methods.class.js");
const hooks = require("./public-methods.hooks");

module.exports = function(app) {
  const paginate = app.get("paginate");

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/public-methods", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("public-methods");

  service.hooks(hooks);
};
