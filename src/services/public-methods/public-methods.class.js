const { registrationParams, clusterTypes } = require("../../api/data-values");

/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }

  async create(data, params) {
    const { method, query } = data;
    let results;
    switch (method) {
      case "get_registration_params":
        results = registrationParams;
        break;
      case "get_cluster_types":
        results = clusterTypes;
        break;
    }

    return results;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
