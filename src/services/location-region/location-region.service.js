// Initializes the `location-region` service on path `/location-region`
const createService = require("feathers-sequelize");
const createModel = require("../../models/location-region.model");
const hooks = require("./location-region.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/location-region", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("location-region");

  service.hooks(hooks);
};
