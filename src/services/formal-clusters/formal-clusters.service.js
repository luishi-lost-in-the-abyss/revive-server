// Initializes the `formal-clusters` service on path `/formal-clusters`
const createService = require("feathers-sequelize");
const createModel = require("../../models/formal-clusters.model");
const hooks = require("./formal-clusters.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/formal-clusters", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("formal-clusters");

  service.hooks(hooks);
};
