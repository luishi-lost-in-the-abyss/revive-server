const { authenticate } = require("@feathersjs/authentication").hooks;
const locationJoin = require("../../hooks/locationJoin");
const Sequelize = require("sequelize");
const authRoleBoolean = require("../../hooks/role-based-auth-boolean");

const innerJoinFunction = async function(context) {
  const Users = context.app.services["users"].Model;
  const FarmPlots = context.app.services["farm-plots"].Model;
  const Clusters = context.app.services["clusters"].Model;
  const FormalClusters = context.app.services["formal-clusters"].Model;

  const query = context.params.query;

  const FarmPlotsModel = { model: FarmPlots, include: [locationJoin(context)] };

  let technician, supervisor;
  if (authRoleBoolean({ technician: true })(context)) {
    technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    FarmPlotsModel.where = { technicianId: technician[0].id };
  } else if (authRoleBoolean({ supervisor: true })(context)) {
    supervisor = await context.app
      .service("supervisors")
      .find({ query: { userId: context.params.payload.userId } });
    supervisor = supervisor.data[0].id;
    technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { supervisorId: supervisor }
    });
    FarmPlotsModel.where = {
      technicianId: { $in: technician.map(({ id }) => id) }
    };
  }
  context.params.sequelize = {
    raw: false,
    include: [
      Object.assign({
        model: Users,
        attributes: ["email"]
      }),
      FarmPlotsModel,
      { model: Clusters, attributes: ["code"] }
    ],
    attributes: [
      ...Object.keys(FormalClusters.attributes).map(key => key),
      [
        Sequelize.fn(
          "concat",
          Sequelize.col("formal_clusters.firstName"),
          " ",
          Sequelize.col("formal_clusters.middleName"),
          " ",
          Sequelize.col("formal_clusters.lastName")
        ),
        "name"
      ]
    ]
  };
  return context;
};

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [innerJoinFunction],
    get: [],
    create: [innerJoinFunction],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
