const { authenticate } = require("@feathersjs/authentication").hooks;
const { disallow, isProvider } = require("feathers-hooks-common");
const authRole = require("../../hooks/role-based-auth");
const authRoleBool = require("../../hooks/role-based-auth-boolean");
const disablePagination = require("../../hooks/disablePagination");
const errors = require("@feathersjs/errors");
const { validateFarmPlots } = require("../../hooks/utility");
const locationJoin = require("../../hooks/locationJoin");
const Sequelize = require("sequelize");

const filterCommodityOthers = async function(context) {
  if (context.data.commodity === "others" && !!context.data.commodityOthers) {
    context.data.commodity = context.data.commodityOthers;
  }
};

const innerJoinFunction = async function(context) {
  const Technicians = context.app.services["technicians"].Model;
  const Supervisors = context.app.services["supervisors"].Model;
  const Users = context.app.services["users"].Model;
  const Participants = context.app.services["participants"].Model;
  const Clusters = context.app.services["clusters"].Model;
  const Fec = context.app.services["fec"].Model;

  const ParticipantsModel = {
    model: Participants,
    attributes: [
      "firstName",
      "middleName",
      "lastName",
      [
        Sequelize.fn(
          "concat",
          Sequelize.col("firstName"),
          " ",
          Sequelize.col("middleName"),
          " ",
          Sequelize.col("lastName")
        ),
        "name"
      ]
    ]
  };

  const TechnicianModel = {
    model: Technicians,
    attributes: ["id", "name"],
    include: [
      {
        model: Supervisors,
        attributes: ["fecId"],
        include: [{ model: Fec, attributes: ["name"] }]
      },
      { model: Users, attributes: ["username"] }
    ]
  };
  let technician, supervisor;
  if (authRoleBool({ technician: true })(context)) {
    technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    TechnicianModel.where = { id: technician[0].id };
  } else if (authRoleBool({ supervisor: true })(context)) {
    supervisor = await context.app
      .service("supervisors")
      .find({ query: { userId: context.params.payload.userId } });
    supervisor = supervisor.data[0].id;
    technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { supervisorId: supervisor }
    });
    TechnicianModel.where = { id: { $in: technician.map(({ id }) => id) } };
  }

  const ClusterModel = {
    model: Clusters,
    attributes: ["code"]
  };

  context.params.sequelize = {
    raw: false,
    include: [
      ParticipantsModel,
      TechnicianModel,
      locationJoin(context),
      ClusterModel
    ]
  };
  return context;
};

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [
      innerJoinFunction,
      authRole({
        admin: true,
        super_admin: true,
        supervisor: true,
        technician: true
      })
    ],
    get: [
      innerJoinFunction,
      authRole({
        admin: true,
        super_admin: true,
        supervisor: true,
        technician: true
      })
    ],
    create: [
      authRole({ admin: true, super_admin: true, supervisor: true }),
      async function(context) {
        if (isProvider("server")(context)) return context;
        context.data.clusterId = context.data.clusterId
          ? context.data.clusterId
          : null;

        validateFarmPlots([context.data]);
      },
      filterCommodityOthers
    ],
    update: [authRole({ admin: true, super_admin: true })],
    patch: [
      authRole({ admin: true, super_admin: true, supervisor: true }),
      async function(context) {
        context.data.clusterId = context.data.clusterId
          ? context.data.clusterId
          : null;
      },
      filterCommodityOthers
    ],
    remove: [authRole({ admin: true, super_admin: true })]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
