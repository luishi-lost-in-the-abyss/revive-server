// Initializes the `farm_plots` service on path `/farm-plots`
const createService = require("feathers-sequelize");
const createModel = require("../../models/farm_plots.model");
const hooks = require("./farm_plots.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/farm-plots", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("farm-plots");

  service.hooks(hooks);
};
