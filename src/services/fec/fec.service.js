// Initializes the `fec` service on path `/fec`
const createService = require("feathers-sequelize");
const createModel = require("../../models/fec.model");
const hooks = require("./fec.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/fec", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("fec");

  service.hooks(hooks);
};
