const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const disablePagination = require("../../hooks/disablePagination");

const adminOnly = authRole({ super_admin: true, admin: true });

const innerJoinFunction = function(context) {
  const Supervisors = context.app.services["supervisors"].Model;

  context.params.sequelize = {
    raw: false,
    include: [{ model: Supervisors }]
  };
  return context;
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        technician: true,
        dispatcher: true,
        supervisor: true
      })
    ],
    find: [
      disablePagination,
      innerJoinFunction,
      async function(context) {
        if (context.params.query && context.params.query.getOwnFec) {
          const user = await context.app
            .service("users")
            .get(context.params.payload.userId);
          context.params.query.fecId = await context.app
            .service(`${user.role}s`)
            .find({
              paginate: false,
              query: { userId: context.params.payload.userId }
            });
          context.result = context.params.query.fecId[0]
            ? [
                await context.app
                  .service("fec")
                  .get(context.params.query.fecId[0].fecId)
              ]
            : [];
          delete context.params.query.getOwnFec;
        }
      }
    ],
    get: [innerJoinFunction],
    create: [adminOnly],
    update: [disallow("external")],
    patch: [adminOnly],
    remove: [adminOnly]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
