const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const authRoleBool = require("../../hooks/role-based-auth-boolean");
const disablePagination = require("../../hooks/disablePagination");
const errors = require("@feathersjs/errors");
const locationJoin = require("../../hooks/locationJoin");
const Sequelize = require("sequelize");
const fs = require("fs");
const jwt = require("jsonwebtoken");

const innerJoinFunction = async function(context) {
  const Participants = context.app.services["participants"].Model;
  const Users = context.app.services["users"].Model;
  const FarmPlots = context.app.services["farm-plots"].Model;
  const SocioInfo = context.app.services["socio-info"].Model;
  const Clusters = context.app.services["clusters"].Model;
  const FormalClusters = context.app.services["formal-clusters"].Model;
  const Technicians = context.app.services["technicians"].Model;

  const query = context.params.query;
  const innerFilter = { Users: null };
  if (query && query.userStatus) {
    innerFilter.Users = Object.assign({}, innerFilter.Users, {
      status: query.userStatus
    });
    delete context.params.query["userStatus"];
  }
  if (query && query.userRole) {
    innerFilter.Users = Object.assign({}, innerFilter.Users, {
      role: query.userRole
    });
    delete context.params.query["userRole"];
  }

  const FarmPlotsModel = {
    model: FarmPlots,
    include: [
      locationJoin(context),
      {
        model: Clusters,
        attributes: ["code"],
        include: [{ model: FormalClusters, attributes: ["id"] }],
        required: false
      }
    ]
  };
  let supervisor, technician;
  if (authRoleBool({ technician: true })(context)) {
    technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    FarmPlotsModel.where = { technicianId: technician[0].id };
  } else if (authRoleBool({ supervisor: true })(context)) {
    supervisor = await context.app
      .service("supervisors")
      .find({ query: { userId: context.params.payload.userId } });
    supervisor = supervisor.data[0].id;
    technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { supervisorId: supervisor }
    });
    if (innerFilter.Users && innerFilter.Users.status === "pending") {
      //this is for pending applications
      context.params.query.createdById = {
        $in: technician.map(({ id }) => id)
      };
    } else {
      FarmPlotsModel.where = {
        technicianId: { $in: technician.map(({ id }) => id) }
      };
    }
  }
  if (context.method === "get") {
    FarmPlotsModel.include.push({
      model: Technicians,
      attributes: ["name", "supervisorId"],
      include: [{ model: Users, attributes: ["username"] }]
    });
  }

  if (query && query.clusterId) {
    FarmPlotsModel.where = Object.assign({}, FarmPlotsModel.where, {
      clusterId: query.clusterId
    });
    delete query.clusterId;
  }

  context.params.sequelize = {
    raw: false,
    include: [
      Object.assign(
        {
          model: Users,
          attributes: ["email", "username", "status"]
        },
        innerFilter.Users ? { where: innerFilter.Users } : null
      ),
      FarmPlotsModel,
      { model: SocioInfo },
      {
        model: FormalClusters,
        attributes: ["id"],
        include: [{ model: Clusters, attributes: ["code"] }]
      },
      { model: Clusters, as: "Leader", attributes: ["code"] }
    ],
    attributes: [
      ...Object.keys(Participants.attributes).map(key => key),
      [
        Sequelize.fn(
          "concat",
          Sequelize.col("participants.firstName"),
          " ",
          Sequelize.col("participants.middleName"),
          " ",
          Sequelize.col("participants.lastName")
        ),
        "name"
      ]
    ]
  };
  if (query && query.name) {
    context.params.sequelize.where = {
      $or: [
        Sequelize.where(
          Sequelize.fn(
            "concat",
            Sequelize.col("participants.firstName"),
            " ",
            Sequelize.col("participants.middleName"),
            " ",
            Sequelize.col("participants.lastName")
          ),
          query.name
        )
      ]
    };
  }
  return context;
};

const checkSupervisor = async function(context) {
  const old = await context.app.service("participants").get(context.id);
  if (authRoleBool({ supervisor: true })(context)) {
    //check if participant is under a technician of the supervisor
    const supervisorUserId = await context.app
      .service("supervisors")
      .find({ query: { userId: context.params.payload.userId } });
    let checkIfTechnicianSupervisor = false;
    old.farm_plots.map(({ technician }) => {
      if (technician.supervisorId === supervisorUserId.data[0].id) {
        checkIfTechnicianSupervisor = true;
      }
    });
    if (!checkIfTechnicianSupervisor) {
      throw new errors.NotAuthenticated();
    }
  }
};

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [
      disablePagination,
      innerJoinFunction,
      authRole({
        admin: true,
        super_admin: true,
        technician: true,
        supervisor: true
      })
    ],
    get: [innerJoinFunction],
    create: [innerJoinFunction],
    update: [],
    patch: [
      checkSupervisor,
      async function(context) {
        //this hook is only for editing the participant entire details i.e. edit_participant page
        if (!context.data.patchIndividualParticipant) return context;

        const participant = Object.assign({}, context.data);
        const old = await context.app.service("participants").get(context.id);

        //need do some processing here
        if (participant.clusterId) {
          //check if same as the leader Id, if same means never change the cluster, if different need to take away the cluster leader id
          participant.clusterLeaderId =
            participant.clusterId === old.clusterLeaderId
              ? participant.clusterId
              : null;
        } else {
          participant.clusterLeaderId = null;
        }

        if (old.user.email !== participant.email) {
          await context.app
            .service("users")
            .patch(old.userId, { email: participant.email });
        }

        if (participant.password) {
          //changing his password
          await context.app
            .service("users")
            .patch(old.userId, { password: participant.password });
          delete participant.password;
          delete participant.confirmPassword;
        }
        context.data = participant;
      }
    ],
    remove: [
      authRole({
        admin: true,
        super_admin: true,
        supervisor: true
      }),
      checkSupervisor
    ]
  },

  after: {
    all: [],
    find: [],
    get: [
      function(context) {
        //restrict to get own user only if not admin
        const isAdmin = authRoleBool({
          admin: true,
          super_admin: true,
          server: true
        })(context);
        if (isAdmin) return context;
        context.result =
          context.params.payload.userId === context.result.userId
            ? context.result
            : {};

        return context;
      }
    ],
    create: [
      async function(context) {
        if (context.data && context.data.file) {
          context.result.dataValues.fileUploadToken = jwt.sign(
            { uploadType: "farmerId", uploadTypeId: context.result.id },
            context.app.get("authentication").secret
          );
        }
      }
    ],
    update: [],
    patch: [
      async function(context) {
        if (context.data && context.data.file) {
          context.result.fileUploadToken = jwt.sign(
            { uploadType: "farmerId", uploadTypeId: context.id },
            context.app.get("authentication").secret
          );
        }
      }
    ],
    remove: [
      async function(context) {
        fs.unlink(`./uploads/farmer/${context.id}.jpg`, err => {
          if (err) return;
        });
      }
    ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
