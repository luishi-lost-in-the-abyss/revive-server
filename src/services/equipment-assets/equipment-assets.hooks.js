const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const authRoleBoolean = require("../../hooks/role-based-auth-boolean");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const { DateTime, Interval } = require("luxon");

const adminDispatcherOnly = authRole({
  super_admin: true,
  admin: true,
  dispatcher: true
});

const innerJoinFunction = function(context) {
  const Equipment = context.app.services["equipment"].Model;
  const AssetMaintenance =
    context.app.services["equipment-asset-maintenance"].Model;
  const FEC = context.app.services["fec"].Model;
  const Bookings = context.app.services["bookings"].Model;
  const currentDate = new Date();

  const AssetMaintenanceObj = {
    model: AssetMaintenance,
    required: false,
    where: { endDate: { $gte: currentDate } },
    order: [["startDate", "ASC"]]
  };
  if (context.params.query.maintenanceOnly) {
    AssetMaintenanceObj.where = {
      startDate: { $lte: currentDate },
      endDate: { $gte: currentDate }
    };
    AssetMaintenanceObj.required = true;
    delete context.params.query.maintenanceOnly;
  }
  context.params.sequelize = {
    raw: false,
    include: [
      { model: Equipment },
      { model: FEC },
      AssetMaintenanceObj,
      {
        model: Bookings,
        required: false,
        where: { startDate: { $gte: currentDate } },
        order: [["startDate", "ASC"]]
      }
    ]
  };
  return context;
};

const restrictDispatcherFec = async function(context) {
  //limit to own fec searches for dispatcher
  if (authRoleBoolean({ dispatcher: true })(context)) {
    const dispatcher = await context.app.service(`dispatchers`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    context.params.query = Object.assign({}, context.params.query, {
      fecId: dispatcher[0].fecId
    });
  }
};

const setAssetCondition = function(obj) {
  if (obj.condition === "retired") {
    obj.conditionText = "retired";
    obj.dataValues.conditionText = "retired";
    return;
  }
  let underMaintenence = false;
  const currentDate = DateTime.fromJSDate(new Date());
  obj.equipment_asset_maintenances.map(({ startDate, endDate }) => {
    const interval = Interval.fromDateTimes(
      new Date(startDate),
      new Date(endDate)
    );
    if (interval.contains(currentDate)) {
      underMaintenence = true;
    }
  });

  obj.conditionText = underMaintenence ? "Under Maintenance" : "Working";
  obj.dataValues.conditionText = underMaintenence
    ? "Under Maintenance"
    : "Working";
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        technician: true,
        dispatcher: true
      })
    ],
    find: [innerJoinFunction, restrictDispatcherFec],
    get: [innerJoinFunction, restrictDispatcherFec],
    create: [adminDispatcherOnly],
    update: [disallow("external")],
    patch: [adminDispatcherOnly],
    remove: [adminDispatcherOnly]
  },

  after: {
    all: [],
    find: [
      async function(context) {
        context.result.map && context.result.map(setAssetCondition);
        context.result.data &&
          context.result.data.map &&
          context.result.data.map(setAssetCondition);
      }
    ],
    get: [
      async function(context) {
        setAssetCondition(context.result);
      }
    ],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
