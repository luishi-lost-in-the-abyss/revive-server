// Initializes the `equipment-assets` service on path `/equipment-assets`
const createService = require("feathers-sequelize");
const createModel = require("../../models/equipment-assets.model");
const hooks = require("./equipment-assets.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate,
    id: "serial"
  };

  // Initialize our service with any options it requires
  app.use("/equipment-assets", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("equipment-assets");

  service.hooks(hooks);
};
