const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const authRoleBool = require("../../hooks/role-based-auth-boolean");
const errors = require("@feathersjs/errors");
const { disallow } = require("feathers-hooks-common");
const disablePagination = require("../../hooks/disablePagination");

const adminOnly = authRole({ super_admin: true, admin: true });

const innerJoinFunction = function(context) {
  const Equipment = context.app.services["equipment"].Model;
  const ServiceCategory = context.app.services["service-category"].Model;

  context.params.sequelize = {
    raw: false,
    include: [{ model: Equipment }, { model: ServiceCategory }]
  };
  return context;
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        technician: true,
        dispatcher: true
      })
    ],
    find: [disablePagination, innerJoinFunction],
    get: [innerJoinFunction],
    create: [
      adminOnly,
      async function(context) {
        //check if have equipment
        const { equipment = [] } = context.data;
        context.params.sequelize = {};
        /*
        if (equipment.length === 0)
          throw new errors.BadRequest("insufficient equipment");
          */
      }
    ],
    update: [disallow("external")],
    patch: [
      authRole({
        super_admin: true,
        admin: true,
        dispatcher: true
      }),
      async function(context) {
        //check if code exist or if it is own code
        if (authRoleBool({ dispatcher: true })(context)) {
          //dispatcher not allowed to edit prices
          delete context.data.price;
        }
        try {
          const service = await context.app
            .service("services")
            .find({ query: { code: context.data.code } });
          if (service.total > 0 && service.data[0].id != context.id)
            throw new Error();
        } catch (err) {
          throw new errors.Conflict("duplicateCode");
        }
      }
    ],
    remove: [adminOnly]
  },

  after: {
    all: [],
    find: [],
    get: [
      async function(context) {
        let serviceCategory = context.result.service_category;
        const parentCategories = [];
        if (serviceCategory) {
          parentCategories.push(serviceCategory.dataValues);
          while (serviceCategory.parentCategoryId) {
            serviceCategory = await context.app
              .service("service-category")
              .get(serviceCategory.parentCategoryId);
            parentCategories.push(serviceCategory);
          }
        }
        context.result = Object.assign({}, context.result.dataValues, {
          parent_category: parentCategories.reverse()
        });
        return context;
      }
    ],
    create: [
      async function(context) {
        const { id } = context.result;
        const { equipment } = context.data;
        if (equipment) {
          await context.app.service("service-equipment").create(
            equipment.map(equipmentId => ({
              serviceId: id,
              equipmentId
            }))
          );
        }
      }
    ],
    update: [],
    patch: [
      async function(context) {
        //need to delete and add the equipments
        const service = await context.app.service("services").get(context.id);
        const oldEquipment = service.equipment.map(equip => equip.id);
        const equipment = context.data.equipment.map(id => parseInt(id));
        const toDelete = oldEquipment.filter(id => equipment.indexOf(id) < 0);
        //delete equipments
        if (toDelete.length > 0) {
          await context.app.service("service-equipment").remove(null, {
            query: { equipmentId: { $in: toDelete }, serviceId: context.id }
          });
        }

        const toAdd = equipment.filter(id => oldEquipment.indexOf(id) < 0);
        //add new equipments
        if (toAdd.length > 0) {
          await context.app.service("service-equipment").create(
            toAdd.map(equipmentId => ({
              serviceId: context.id,
              equipmentId
            }))
          );
        }
      }
    ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
