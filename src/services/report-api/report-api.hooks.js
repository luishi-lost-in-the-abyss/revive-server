const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const authRoleBoolean = require("../../hooks/role-based-auth-boolean");
const errors = require("@feathersjs/errors");
const { iff } = require("feathers-hooks-common");
const { DateTime } = require("luxon");

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        super_admin: true,
        admin: true,
        dispatcher: true,
        technician: true,
        supervisor: true
      })
    ],
    find: [],
    get: [],
    create: [
      iff(authRoleBoolean({ technician: true, supervisor: true }), [
        async function(context) {
          const { report, query } = context.data;
          switch (report) {
            case "rawdata_participant":
            case "rawdata_participants":
            case "rawdata_farm_plots":
            case "rawdata_formal_clusters":
            case "rawdata_cluster":
              context.data.useCurrentParams = context.params;
              break;
            default:
              throw new errors.MethodNotAllowed();
          }
        }
      ]),
      iff(
        authRoleBoolean({ super_admin: true, admin: true, dispatcher: true }),
        [
          async function(context) {
            const { report, query } = context.data;
            switch (report) {
              case "rawdata_participant":
              case "rawdata_participants":
              case "rawdata_farm_plots":
              case "rawdata_formal_clusters":
              case "rawdata_cluster":
                authRole({ super_admin: true, admin: true })(context);
              case "rawdata_services":
              case "rawdata_equipment":
              case "rawdata_equipment_assets":
                break;
              case "rawdata_equipment_maintenance":
              case "service_performance":
              case "booking_schedule":
              case "booking_equipment_assets":
                if (authRoleBoolean({ dispatcher: true })(context)) {
                  query.fecId = await context.app
                    .service("dispatchers")
                    .find({ query: { userId: context.params.payload.userId } });
                  query.fecId = query.fecId.data[0].fecId;
                }
                query.startDate = DateTime.fromISO(query.startDate)
                  .startOf("day")
                  .toJSDate();
                query.endDate = DateTime.fromISO(query.endDate)
                  .endOf("day")
                  .toJSDate();
                break;
              default:
                throw new errors.MethodNotAllowed();
            }
          }
        ]
      )
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
