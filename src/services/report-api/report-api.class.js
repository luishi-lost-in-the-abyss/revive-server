const { DateTime, Interval } = require("luxon");
/* eslint-disable no-unused-vars */

const extractLocationDetails = function(barangayObj) {
  const barangay = barangayObj;
  const municipality = barangay.municipality;
  const province = municipality.province;
  const region = province.region;
  const country = region.country;
  return { barangay, municipality, province, region, country };
};
const participantHeader = [
  "id",
  "first name",
  "middle name",
  "last name",
  "birthdate",
  "occupation",
  "civil",
  "contact",
  "operator",
  "gender",
  "resident address",
  "bank name",
  "bank account type",
  "bank account name",
  "bank tin no.",
  "health conditions",
  "clusterId"
];
const getParticipantRow = function(participant) {
  return [
    participant.id,
    participant.firstName,
    participant.middleName,
    participant.lastName,
    participant.birthdate,
    participant.occupation,
    participant.civil,
    participant.contact,
    participant.operator,
    participant.gender,
    participant.residentAddress,
    participant.bankName,
    participant.bankAccType,
    participant.bankAccName,
    participant.bankTinNo,
    participant.healthConditions.join(", ") + " " + participant.healthOthers,
    participant.clusterId
  ];
};
const formalClusterHeader = [
  "id",
  "first name",
  "middle name",
  "last name",
  "contact",
  "operator",
  "gender",
  "bank name",
  "bank account type",
  "bank account name",
  "bank tin no.",
  "clusterId"
];
const getFormalCLusterRow = function(participant) {
  return [
    participant.id,
    participant.firstName,
    participant.middleName,
    participant.lastName,
    participant.contact,
    participant.operator,
    participant.gender,
    participant.bankName,
    participant.bankAccType,
    participant.bankAccName,
    participant.bankTinNo,
    participant.clusterId
  ];
};

const farmPlotHeader = [
  "id",
  "code",
  "farmer name",
  "description",
  "informal cluster name",
  "fec",
  "type",
  "engagement scheme",
  "country",
  "region",
  "province",
  "municipality",
  "barangay",
  "purok",
  "tenure",
  "commodity",
  "total area",
  "plantable area",
  "water system",
  "coordinates",
  "previous buyer",
  "previous payment mode",
  "source of finance",
  "crop insurance",
  "technicianId",
  "technician"
];
const getFarmPlotRow = function(plot) {
  const locationObj = extractLocationDetails(plot.barangay);
  return [
    plot.id,
    plot.code,
    `${plot.participant ? plot.participant.dataValues.name : ""}`,
    plot.description,
    plot.cluster ? plot.cluster.code : "",
    plot.technician ? plot.technician.supervisor.fec.name : "",
    plot.type,
    plot.engagementScheme,
    locationObj.country.name,
    locationObj.region.name,
    locationObj.province.name,
    locationObj.municipality.name,
    locationObj.barangay.name,
    plot.purok,
    plot.tenure,
    plot.commodity,
    parseFloat(plot.totalArea).toFixed(2),
    parseFloat(plot.plantableArea).toFixed(2),
    plot.waterSystem,
    plot.coordinates,
    plot.prevBuyer,
    plot.prevPaymentMode,
    plot.sourceFinance,
    (!!plot.cropInsurance).toString(),
    plot.technicianId,
    plot.technician ? plot.technician.name : "-"
  ];
};

class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }

  async create(data, params) {
    const { report, query, paginate } = data;
    let result,
      equipment,
      assets,
      services,
      participant,
      cluster,
      fec,
      booking,
      plots,
      tempObj;

    let fecName = {};
    fec = await this.app.service("fec").find({ paginate: false });
    fec.map(({ id, name }) => {
      fecName[id] = name;
    });

    switch (report) {
      case "rawdata_participants":
        participant = await this.app
          .service("participants")
          .find(
            Object.assign(
              {},
              data.useCurrentParams ? params : {},
              paginate
                ? { query: Object.assign({}, query, paginate) }
                : { paginate: false }
            )
          );
        result = { header: [participantHeader], values: [] };
        if (paginate) {
          result.paginate = {
            total: participant.total,
            limit: participant.limit,
            skip: participant.skip
          };
        }
        (participant.data ? participant.data : participant).map(obj => {
          result.values.push(getParticipantRow(obj));
        });
        break;
      case "rawdata_participant":
        participant = await this.app
          .service("participants")
          .get(query.participantId, data.useCurrentParams ? params : {});
        result = [
          ["id", participant.id],
          ["first name", participant.firstName],
          ["middle name", participant.middleName],
          ["last name", participant.lastName],
          ["birthdate", participant.birthdate],
          ["occupation", participant.occupation],
          ["civil", participant.civil],
          ["contact", participant.contact],
          ["operator", participant.operator],
          ["gender", participant.gender],
          ["resident address", participant.residentAddress],
          ["bank name", participant.bankName],
          ["bank account type", participant.bankAccType],
          ["bank account name", participant.bankAccName],
          ["bank tin no.", participant.bankTinNo],
          [
            "health conditions",
            participant.healthConditions.join(", ") +
              " " +
              participant.healthOthers
          ],
          ["clusterId", participant.clusterId],
          [],
          ["farm plots"],
          farmPlotHeader
        ];
        participant.farm_plots.map(plot => {
          result.push(getFarmPlotRow(plot));
        });
        break;

      case "rawdata_formal_clusters":
        participant = await this.app
          .service("formal-clusters")
          .find(
            Object.assign(
              {},
              data.useCurrentParams ? params : {},
              paginate
                ? { query: Object.assign({}, query, paginate) }
                : { paginate: false }
            )
          );
        result = { header: [formalClusterHeader], values: [] };
        if (paginate) {
          result.paginate = {
            total: participant.total,
            limit: participant.limit,
            skip: participant.skip
          };
        }
        (participant.data ? participant.data : participant).map(obj => {
          result.values.push(getFormalCLusterRow(obj));
        });
        break;
      case "rawdata_farm_plots":
        plots = await this.app
          .service("farm-plots")
          .find(
            Object.assign(
              {},
              data.useCurrentParams ? params : {},
              paginate
                ? { query: Object.assign({}, query, paginate) }
                : { paginate: false }
            )
          );
        result = { header: [farmPlotHeader], values: [] };
        if (paginate) {
          result.paginate = {
            total: plots.total,
            limit: plots.limit,
            skip: plots.skip
          };
        }
        (plots.data ? plots.data : plots).map(obj => {
          result.values.push(getFarmPlotRow(obj));
        });
        break;
      case "rawdata_cluster":
        cluster = await this.app
          .service("clusters")
          .get(query.clusterId, data.useCurrentParams ? params : {});
        result = [
          ["id", cluster.id],
          ["code", cluster.code],
          [
            "leader",
            cluster.Leader
              ? `${cluster.Leader.firstName} ${cluster.Leader.middleName} ${cluster.Leader.lastName}`
              : "-"
          ],
          ["type", cluster.formal_cluster ? "formal" : "informal"],
          [],
          ["participants"],
          participantHeader
        ];
        (cluster.formal_cluster
          ? cluster.formal_cluster.participants
          : cluster.farm_plots.map(({ participant }) => participant)
        ).map(participant => {
          result.push(getParticipantRow(participant));
        });

        if (cluster.formal_cluster) {
          //show formal cluster participant profile
          result.push([]);
          result.push(["formal cluster profile"]);
          participant = cluster.formal_cluster;
          [
            ["first name", participant.firstName],
            ["middle name", participant.middleName],
            ["last name", participant.lastName],
            ["contact", participant.contact],
            ["operator", participant.operator],
            ["gender", participant.gender],
            ["bank name", participant.bankName],
            ["bank account type", participant.bankAccType],
            ["bank account name", participant.bankAccName],
            ["bank tin no.", participant.bankTinNo]
          ].map(arr => {
            result.push(arr);
          });
          //show all farm plots
          result.push([]);
          result.push(["farm plots"]);
          result.push(farmPlotHeader);
          cluster.formal_cluster.farm_plots.map(plot => {
            result.push(getFarmPlotRow(plot));
          });
        }
        break;

      case "rawdata_services":
        services = await this.app.service("services").find({ paginate: false });
        result = [
          [
            "id",
            "code",
            "name",
            "description",
            "price",
            "phase",
            "equipment",
            "equipmentIds"
          ]
        ];
        services.map(
          ({
            id,
            code,
            name,
            description,
            price,
            equipment,
            service_category
          }) => {
            result.push([
              id,
              code,
              name,
              description,
              price,
              service_category.name,
              equipment.map(({ name }) => name).join(", "),
              equipment.map(({ id }) => id).join(", ")
            ]);
          }
        );
        break;
      case "rawdata_equipment":
        equipment = await this.app
          .service("equipment")
          .find({ paginate: false });
        result = [["id", "name", "description", "number of assets"]];
        equipment.map(({ id, name, description, equipment_assets }) => {
          result.push([id, name, description, equipment_assets.length]);
        });
        break;
      case "rawdata_equipment_assets":
        assets = await this.app
          .service("equipment-assets")
          .find({ paginate: false, query: {} });
        result = [
          ["serial", "condition", "fecId", "fec", "equipmentId", "equipment"]
        ];
        assets.map(({ serial, conditionText, equipment, fec }) => {
          result.push([
            serial,
            conditionText,
            fec.id,
            fec.name,
            equipment.id,
            equipment.name
          ]);
        });
        break;
      case "booking_schedule":
        booking = await this.app.service("bookings").find({
          paginate: false,
          query: {
            $or: [
              { startDate: { $between: [query.startDate, query.endDate] } },
              { endDate: { $between: [query.startDate, query.endDate] } }
            ]
          }
        });
        result = [
          [
            "ID",
            "Booked Date From",
            "Booked Date To",
            "Booked Time Start",
            "Booked Time End",
            "Actual Date From",
            "Actual Date To",
            "Actual Time Start",
            "Actual Time End",
            "Operating Time, Hr",
            "Capacity, Hr / Ha",
            "Participant",
            "Requestor",
            "Commodity",
            "Farm Activity (Phase)",
            "Service",
            "Service Code",
            "Equipment (Serial)",
            "FEC",
            "Booked Area, Ha",
            "Completed Area Ha",
            "Charge per Ha.",
            "Additional Charges",
            "Discount",
            "Final Price",
            "Plot Code",
            "Province",
            "Municipality",
            "Barangay",
            "Purok/Sitio",
            "Status",
            "Remarks",
            "Cluster Booking"
          ]
        ];
        booking.map(obj => {
          const {
            id,
            dataSnapshot,
            startDate,
            endDate,
            actualStartDate,
            actualEndDate,
            status,
            statusReason,
            serviceArea,
            actualCompletedArea,
            additionalCosts,
            discount,
            technician,
            individualInformal
          } = obj;
          const { booking } = dataSnapshot;
          const { service } = booking;
          const startDateObj = DateTime.fromJSDate(startDate);
          const endDateObj = DateTime.fromJSDate(endDate);
          const actualStartDateObj = actualStartDate
            ? DateTime.fromJSDate(actualStartDate)
            : null;
          const actualEndDateObj = actualEndDate
            ? DateTime.fromJSDate(actualEndDate)
            : null;
          const location = extractLocationDetails(booking.farm_plot.barangay);
          const isFormal = !!booking.farm_plot.formalClusterId;
          const participant = isFormal
            ? booking.farm_plot.formal_cluster
            : booking.farm_plot.participant;
          const additionalCostsTotal = additionalCosts
            ? additionalCosts.reduce(
                (a, { amount }) => a + parseFloat(amount),
                0
              )
            : 0;
          const costBeforeDiscount =
            additionalCostsTotal +
            (actualCompletedArea ? actualCompletedArea : serviceArea) *
              service.price;
          const costAfterDiscount = (1 - discount / 100) * costBeforeDiscount;
          const duration = Interval.fromDateTimes(
            actualStartDateObj ? actualStartDateObj : startDateObj,
            actualEndDateObj ? actualEndDateObj : endDateObj
          )
            .toDuration("minutes")
            .minutes.toFixed(0);
          result.push([
            id,
            startDateObj.toFormat("dd/MM/yy"),
            endDateObj.toFormat("dd/MM/yyyy"),
            startDateObj.toFormat("H:mm:ss"),
            endDateObj.toFormat("H:mm:ss"),
            actualStartDateObj ? actualStartDateObj.toFormat("dd/MM/yy") : "",
            actualEndDateObj ? actualEndDateObj.toFormat("dd/MM/yyyy") : "",
            actualStartDateObj ? actualStartDateObj.toFormat("H:mm:ss") : "",
            actualEndDateObj ? actualEndDateObj.toFormat("H:mm:ss") : "",
            (duration / 60).toFixed(2),
            (
              duration /
              60 /
              parseFloat(
                actualCompletedArea ? actualCompletedArea : serviceArea
              )
            ).toFixed(2),
            `${participant.firstName} ${participant.middleName} ${participant.lastName}`,
            technician ? technician.name : "SYSTEM",
            service.service_category.parent_category
              ? service.service_category.parent_category.name
              : service.service_category.name,
            service.service_category.parent_category
              ? service.service_category.name
              : "",
            service.name,
            service.code,
            booking.equipment_assets
              .map(({ serial, equipment }) => `${equipment.name} (${serial})`)
              .join(", "),
            booking.equipment_assets.length > 0
              ? fecName[booking.equipment_assets[0].fecId] +
                ` (ID: ${booking.equipment_assets[0].fecId})`
              : "",
            parseFloat(serviceArea).toFixed(2),
            actualCompletedArea
              ? parseFloat(actualCompletedArea).toFixed(2)
              : "",
            service.price,
            additionalCostsTotal,
            discount,
            costAfterDiscount,
            booking.farm_plot.code,
            location.province.name,
            location.municipality.name,
            location.barangay.name,
            booking.farm_plot.purok,
            status,
            statusReason ? statusReason : "",
            !!(isFormal || (!individualInformal && participant.clusterId))
          ]);
        });
        break;
      case "booking_equipment_assets":
        fec = await this.app.service("fec").find({ paginate: false });
        const fecObj = {};
        fec.map(({ id, name }) => {
          fecObj[id] = name;
        });
        booking = await this.app.service("bookings").find({
          paginate: false,
          query: {
            $or: [
              {
                startDate: { $between: [query.startDate, query.endDate] },
                status: { $in: ["completed", "cancel"] }
              },
              {
                endDate: { $between: [query.startDate, query.endDate] },
                status: { $in: ["completed", "cancel"] }
              }
            ]
          }
        });

        result = [
          [
            "serial",
            "fecId",
            "fec",
            "equipmentId",
            "equipment",
            "Booked Area, Ha",
            "Book Time, Hrs",
            "Actual Area, Ha",
            "Operating Time, Hrs",
            "Bad Weather",
            "Under Repair",
            "Permits and Licensed",
            "Reschedule",
            "Others"
          ]
        ];
        tempObj = {};
        booking.map(obj => {
          const {
            startDate,
            endDate,
            status,
            statusReason,
            actualStartDate,
            actualEndDate,
            serviceArea,
            actualCompletedArea,
            dataSnapshot
          } = obj;
          const { booking } = dataSnapshot;
          if (booking.equipment_assets.length <= 0) return;
          if (
            query.fecId &&
            (booking.equipment_assets.length <= 0 ||
              booking.equipment_assets[0].fecId !== query.fecId)
          ) {
            return;
          }

          const startDateObj = DateTime.fromJSDate(startDate);
          const endDateObj = DateTime.fromJSDate(endDate);
          const actualStartDateObj = actualStartDate
            ? DateTime.fromJSDate(actualStartDate)
            : null;
          const actualEndDateObj = actualEndDate
            ? DateTime.fromJSDate(actualEndDate)
            : null;
          const bookedTime = Interval.fromDateTimes(
            startDateObj,
            endDateObj
          ).count("minutes");
          const actualTime = actualEndDateObj
            ? Interval.fromDateTimes(
                actualStartDateObj,
                actualEndDateObj
              ).count("minutes")
            : 0;

          booking.equipment_assets.map(asset => {
            if (!tempObj[asset.serial]) {
              tempObj[asset.serial] = Object.assign(
                {
                  bookedArea: 0,
                  bookedTime: 0,
                  actualArea: 0,
                  operatingTime: 0,
                  cancel: {
                    "Bad Weather": 0,
                    "Under Repair": 0,
                    "Permits and Licensed Concern": 0,
                    Reschedule: 0,
                    Others: 0
                  }
                },
                asset
              );
            }
            tempObj[asset.serial].bookedArea += parseFloat(serviceArea);
            tempObj[asset.serial].bookedTime += bookedTime;
            if (status === "completed") {
              tempObj[asset.serial].actualArea += parseFloat(
                actualCompletedArea ? actualCompletedArea : serviceArea
              );
              tempObj[asset.serial].operatingTime += actualStartDateObj
                ? actualTime
                : bookedTime;
            } else if (status === "cancel") {
              if (tempObj[asset.serial].cancel[statusReason] >= 0) {
                tempObj[asset.serial].cancel[statusReason] += 1;
              } else {
                tempObj[asset.serial].cancel.Others += 1;
              }
            }
          });
        });
        Object.keys(tempObj).map(serial => {
          const obj = tempObj[serial];
          result.push([
            serial,
            obj.fecId,
            fecObj[obj.fecId],
            obj.equipmentId,
            obj.equipment.name,
            parseFloat(obj.bookedArea).toFixed(2),
            parseFloat(obj.bookedTime / 60).toFixed(2),
            parseFloat(obj.actualArea).toFixed(2),
            parseFloat(obj.operatingTime / 60).toFixed(2),
            obj.cancel["Bad Weather"],
            obj.cancel["Under Repair"],
            obj.cancel["Permits and Licensed Concern"],
            obj.cancel["Reschedule"],
            obj.cancel["Others"]
          ]);
        });
        break;
      case "service_performance":
        booking = await this.app.service("bookings").find({
          paginate: false,
          query: {
            $or: [
              {
                startDate: { $between: [query.startDate, query.endDate] },
                actualStartDate: null,
                status: "completed"
              },
              {
                endDate: { $between: [query.startDate, query.endDate] },
                actualEndDate: null,
                status: "completed"
              }
            ]
          }
        });
        result = [
          [
            "Service Code",
            "Farm Activity",
            "Area, ha",
            "Operating Time, hr",
            "Maximum, hr/ha",
            "Minimum, hr/ha",
            "Average hr/ha"
          ]
        ];

        tempObj = {};

        booking.map(obj => {
          const {
            startDate,
            endDate,
            actualStartDate,
            actualEndDate,
            serviceArea,
            actualCompletedArea,
            dataSnapshot
          } = obj;
          const { booking } = dataSnapshot;
          if (
            query.fecId &&
            (booking.equipment_assets.length <= 0 ||
              booking.equipment_assets[0].fecId !== query.fecId)
          ) {
            return;
          }
          const serviceCode = booking.service.code;
          if (!tempObj[serviceCode]) {
            tempObj[serviceCode] = {
              farmActivity: booking.service.name,
              totalArea: 0,
              totalHours: 0,
              maxHour: 0,
              count: 0,
              minHour: 999999999999
            };
          }
          tempObj[serviceCode].count += 1;
          tempObj[serviceCode].totalArea += parseFloat(
            actualCompletedArea ? actualCompletedArea : serviceArea
          );
          const startDateObj = DateTime.fromJSDate(startDate);
          const endDateObj = DateTime.fromJSDate(endDate);
          const actualStartDateObj = actualStartDate
            ? DateTime.fromJSDate(actualStartDate)
            : null;
          const actualEndDateObj = actualEndDate
            ? DateTime.fromJSDate(actualEndDate)
            : null;
          const duration = Interval.fromDateTimes(
            actualStartDateObj ? actualStartDateObj : startDateObj,
            actualEndDateObj ? actualEndDateObj : endDateObj
          ).count("minutes");
          const hourPerArea =
            duration /
            60 /
            (actualCompletedArea ? actualCompletedArea : serviceArea);

          tempObj[serviceCode].totalHours += duration;
          tempObj[serviceCode].maxHour =
            tempObj[serviceCode].maxHour < hourPerArea
              ? hourPerArea
              : tempObj[serviceCode].maxHour;
          tempObj[serviceCode].minHour =
            tempObj[serviceCode].minHour > hourPerArea
              ? hourPerArea
              : tempObj[serviceCode].minHour;
        });
        Object.keys(tempObj).map(serviceCode => {
          const obj = tempObj[serviceCode];
          result.push([
            serviceCode,
            obj.farmActivity,
            parseFloat(obj.totalArea).toFixed(2),
            parseFloat(obj.totalHours / 60).toFixed(2),
            parseFloat(obj.maxHour).toFixed(2),
            parseFloat(obj.minHour).toFixed(2),
            parseFloat(obj.totalHours / 60 / obj.totalArea).toFixed(2)
          ]);
        });
        break;
      case "rawdata_equipment_maintenance":
        equipment = await this.app.service("equipment-asset-maintenance").find({
          paginate: false,
          query: {
            $or: [
              { startDate: { $gte: query.startDate, $lte: query.endDate } },
              { endDate: { $gte: query.startDate, $lte: query.endDate } }
            ]
          }
        });
        result = [
          [
            "Serial",
            "Equipment",
            "Equipment ID",
            "Start Date",
            "Start Time",
            "End Date",
            "End Time"
          ]
        ];
        equipment.map(
          ({ startDate, endDate, equipmentAssetSerial, equipment_asset }) => {
            const startDateObj = DateTime.fromJSDate(startDate);
            const endDateObj = DateTime.fromJSDate(endDate);
            const { equipment } = equipment_asset;
            result.push([
              equipmentAssetSerial,
              equipment.name,
              equipment.id,
              startDateObj.toFormat("dd/MM/yy"),
              startDateObj.toFormat("H:mm:ss"),
              endDateObj.toFormat("dd/MM/yyyy"),
              endDateObj.toFormat("H:mm:ss")
            ]);
          }
        );
        break;
    }

    return result;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
