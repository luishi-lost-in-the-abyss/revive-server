// Initializes the `report-api` service on path `/report-api`
const createService = require("./report-api.class.js");
const hooks = require("./report-api.hooks");

module.exports = function(app) {
  const paginate = app.get("paginate");

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/report-api", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("report-api");

  service.hooks(hooks);
};
