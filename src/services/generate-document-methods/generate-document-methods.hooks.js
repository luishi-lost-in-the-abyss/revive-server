const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        admin: true,
        super_admin: true,
        supervisor: true,
        technician: true,
        dispatcher: true
      })
    ],
    find: [],
    get: [],
    create: [
      async function(context) {
        const data = context.data;
        const { method, query = {} } = data;
        switch (method) {
          case "generate_purchase":
          case "generate_purchase_informal":
          case "generate_invoice":
          case "generate_invoice_informal":
          case "generate_delivery":
          case "generate_delivery_informal":
          case "generate_price_agreement":
            break;

          default:
            throw new errors.BadRequest("invalid method");
        }
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
