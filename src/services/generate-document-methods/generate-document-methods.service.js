// Initializes the `generate-document-methods` service on path `/generate-document-methods`
const createService = require("./generate-document-methods.class.js");
const hooks = require("./generate-document-methods.hooks");

module.exports = function(app) {
  const paginate = app.get("paginate");

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/generate-document-methods", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("generate-document-methods");

  service.hooks(hooks);
};
