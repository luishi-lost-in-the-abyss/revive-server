const {
  getInvoiceDefinition,
  getPurchaseOrderDefinition,
  getDeliveryDefinition,
  getPriceAgreementDefinition
} = require("../../api/DocumentDefinition");
/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }

  async create(data, params) {
    const { method, query = {} } = data;
    const app = this.app;
    let results, participant, participants, agreement;

    let result = {};
    let booking;
    if (method !== "generate_price_agreement") {
      booking = await this.app.service("bookings").get(query.bookingId);
      //we generate based off the data that has been captured on creation of the
      participants = booking.dataSnapshot.participants;
      booking = Object.assign({}, booking.dataSnapshot.booking, {
        actualCompletedArea: booking.actualCompletedArea,
        actualStartDate: booking.actualStartDate,
        actualEndDate: booking.actualEndDate
      });
    }

    switch (method) {
      case "generate_purchase":
        result.dd = getPurchaseOrderDefinition({
          farm_plots: [booking.farm_plot],
          booking
        });
        break;
      case "generate_purchase_informal":
        participant = participants.filter(
          ({ id }) => query.participantId === id
        )[0];
        participant.farm_plots[0].participant = participant;
        result.dd = getPurchaseOrderDefinition({
          farm_plots: participant.farm_plots,
          booking,
          isInformal: true
        });
        break;
      case "generate_invoice":
        result.dd = getInvoiceDefinition({
          farm_plots: [booking.farm_plot],
          booking
        });
        break;
      case "generate_invoice_informal":
        participant = participants.filter(
          ({ id }) => query.participantId === id
        )[0];
        participant.farm_plots[0].participant = participant;
        result.dd = getInvoiceDefinition({
          farm_plots: participant.farm_plots,
          booking,
          isInformal: true
        });
        break;
      case "generate_delivery":
        result.dd = getDeliveryDefinition({
          farm_plots: [booking.farm_plot],
          booking
        });
        break;
      case "generate_delivery_informal":
        participant = participants.filter(
          ({ id }) => query.participantId === id
        )[0];
        participant.farm_plots[0].participant = participant;
        result.dd = getDeliveryDefinition({
          farm_plots: participant.farm_plots,
          booking,
          isInformal: true
        });
        break;
      case "generate_price_agreement":
        agreement = await this.app
          .service("price-agreements")
          .get(query.agreementId);
        result.dd = getPriceAgreementDefinition({
          agreement
        });
        break;
    }
    return result;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
