// Initializes the `price-agreements` service on path `/price-agreements`
const createService = require("feathers-sequelize");
const createModel = require("../../models/price-agreements.model");
const hooks = require("./price-agreements.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/price-agreements", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("price-agreements");

  service.hooks(hooks);
};
