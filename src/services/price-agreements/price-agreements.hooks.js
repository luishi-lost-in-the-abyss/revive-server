const { authenticate } = require("@feathersjs/authentication").hooks;
const authRoleBoolean = require("../../hooks/role-based-auth-boolean");
const authRole = require("../../hooks/role-based-auth");
const isAdmin = authRole({ admin: true, super_admin: true });
const isAdminTechnician = authRole({
  admin: true,
  super_admin: true,
  technician: true
});
const locationJoin = require("../../hooks/locationJoin");

const innerJoinFunction = function(context) {
  const Bookings = context.app.services["bookings"].Model;

  context.params.sequelize = {
    raw: false,
    include: [{ model: Bookings }]
  };
  return context;
};

const restrictTechnician = async function(context) {
  if (authRoleBoolean({ technician: true })(context)) {
    const technician = await context.app.service(`technicians`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    context.params.sequelize.include[0].where = {
      technicianId: technician[0].id
    };
  }
};

module.exports = {
  before: {
    all: [
      authenticate("jwt"),
      authRole({
        admin: true,
        super_admin: true,
        supervisor: true,
        dispatcher: true,
        technician: true
      })
    ],
    find: [innerJoinFunction, restrictTechnician],
    get: [innerJoinFunction, restrictTechnician],
    create: [isAdminTechnician],
    update: [isAdmin],
    patch: [isAdmin],
    remove: [isAdmin]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
