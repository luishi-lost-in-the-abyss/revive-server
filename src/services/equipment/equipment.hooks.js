const { authenticate } = require("@feathersjs/authentication").hooks;
const disablePagination = require("../../hooks/disablePagination");
const authRoleBoolean = require("../../hooks/role-based-auth-boolean");

const innerJoinFunction = function(context) {
  const EquipmentAssets = context.app.services["equipment-assets"].Model;
  const EquipmentAssetsModel = { model: EquipmentAssets };
  if (context.params.query && context.params.query.fecId) {
    EquipmentAssetsModel.where = { fecId: context.params.query.fecId };
    EquipmentAssetsModel.required = false;
    delete context.params.query.fecId;
  }
  context.params.sequelize = {
    raw: false,
    include: [EquipmentAssetsModel]
  };
};

const restrictDispatcherFec = async function(context) {
  //limit to own fec searches for dispatcher
  if (authRoleBoolean({ dispatcher: true })(context)) {
    const dispatcher = await context.app.service(`dispatchers`).find({
      paginate: false,
      query: { userId: context.params.payload.userId }
    });
    context.params.query = Object.assign({}, context.params.query, {
      fecId: dispatcher[0].fecId
    });
  }
};
module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [disablePagination, restrictDispatcherFec, innerJoinFunction],
    get: [],
    create: [
      restrictDispatcherFec,
      async function(context) {
        if (authRoleBoolean({ dispatcher: true })(context)) {
          context.data.fecId = context.params.query.fecId;
          delete context.params.query.fecId;
        }
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
