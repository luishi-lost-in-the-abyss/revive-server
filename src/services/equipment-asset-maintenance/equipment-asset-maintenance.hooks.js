const { authenticate } = require("@feathersjs/authentication").hooks;
const { DateTime } = require("luxon");
const authRole = require("../../hooks/role-based-auth");

const innerJoinFunction = function(context) {
  const EquipmentAssets = context.app.services["equipment-assets"].Model;
  const Equipment = context.app.services["equipment"].Model;

  context.params.sequelize = {
    raw: false,
    include: [{ model: EquipmentAssets, include: [{ model: Equipment }] }]
  };
  return context;
};

module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [innerJoinFunction],
    get: [innerJoinFunction],
    create: [
      authRole({ admin: true, super_admin: true, dispatcher: true }),
      async function(context) {
        context.data.startDate = DateTime.fromJSDate(
          new Date(context.data.startDate)
        )
          .startOf("day")
          .toJSDate();
        context.data.endDate = DateTime.fromJSDate(
          new Date(context.data.endDate)
        )
          .endOf("day")
          .toJSDate();

        //check for overlaps

        const totalOverlap = await context.app
          .service("equipment-asset-maintenance")
          .find({
            paginate: false,
            query: {
              startDate: { $lte: context.data.startDate },
              endDate: { $gte: context.data.endDate },
              equipmentAssetSerial: context.data.equpimentAssetSerial
            }
          });
        if (totalOverlap.length > 0) {
          context.result = totalOverlap[0];
          return;
        }
        const startOverlap = await context.app
          .service("equipment-asset-maintenance")
          .find({
            paginate: false,
            query: {
              startDate: { $lte: context.data.startDate },
              endDate: {
                $lte: context.data.endDate,
                $gte: context.data.startDate
              },
              equipmentAssetSerial: context.data.equpimentAssetSerial
            }
          });
        if (startOverlap.length > 0) {
          context.result = await await context.app
            .service("equipment-asset-maintenance")
            .patch(startOverlap[0].id, { endDate: context.data.endDate });
          return;
        }

        const endOverlap = await context.app
          .service("equipment-asset-maintenance")
          .find({
            paginate: false,
            query: {
              endDate: { $gte: context.data.endDate },
              startDate: {
                $lte: context.data.endDate,
                $gte: context.data.startDate
              },
              equipmentAssetSerial: context.data.equpimentAssetSerial
            }
          });
        if (endOverlap.length > 0) {
          context.result = await await context.app
            .service("equipment-asset-maintenance")
            .patch(endOverlap[0].id, { startDate: context.data.startDate });
          return;
        }
      }
    ],
    update: [authRole({ admin: true, super_admin: true, dispatcher: true })],
    patch: [authRole({ admin: true, super_admin: true, dispatcher: true })],
    remove: [authRole({ admin: true, super_admin: true, dispatcher: true })]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
