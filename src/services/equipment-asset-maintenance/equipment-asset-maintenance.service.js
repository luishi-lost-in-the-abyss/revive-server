// Initializes the `equipment-asset-maintenance` service on path `/equipment-asset-maintenance`
const createService = require("feathers-sequelize");
const createModel = require("../../models/equipment-asset-maintenance.model");
const hooks = require("./equipment-asset-maintenance.hooks");

module.exports = function(app) {
  const Model = createModel(app);
  const paginate = app.get("paginate");

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/equipment-asset-maintenance", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("equipment-asset-maintenance");

  service.hooks(hooks);
};
