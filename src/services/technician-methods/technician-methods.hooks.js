const { authenticate } = require("@feathersjs/authentication").hooks;
const authRole = require("../../hooks/role-based-auth");
const errors = require("@feathersjs/errors");

module.exports = {
  before: {
    all: [authenticate("jwt"), authRole({ technician: true })],
    find: [],
    get: [],
    create: [
      async function(context) {
        const data = context.data;
        const { method, query = {} } = data;
        let technician, user, participant, plots, jsonObj;
        technician = await context.app
          .service("technicians")
          .find({ query: { userId: context.params.payload.userId } });
        technician = technician.data[0].id;
        switch (method) {
          case "get_cluster_ids":
          case "get_pending_applications":
            context.data.technician = technician;
            break;
          default:
            throw new errors.BadRequest("invalid method");
        }
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
