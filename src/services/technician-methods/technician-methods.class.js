/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  setup(app) {
    this.app = app;
  }
  async create(data, params) {
    const { method, query = {} } = data;
    const app = this.app;
    let results, users, participants, cluster, farmPlots, promises, technicians;
    switch (method) {
      case "get_cluster_ids":
        results = await app.service("farm-plots").find({
          paginate: false,
          query: {
            technicianId: data.technician
          }
        });
        results = [
          ...new Set(
            results
              .filter(({ clusterId }) => !!clusterId)
              .map(({ clusterId }) => clusterId)
          )
        ].concat(
          (await app.service("formal-clusters").find({
            paginate: false,
            query: {
              id: {
                $in: [
                  ...new Set(
                    results
                      .filter(({ formalClusterId }) => !!formalClusterId)
                      .map(({ formalClusterId }) => formalClusterId)
                  )
                ]
              }
            }
          })).map(({ clusterId }) => clusterId)
        );
        break;
    }

    return results;
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
