// Initializes the `technician-methods` service on path `/technician-methods`
const createService = require("./technician-methods.class.js");
const hooks = require("./technician-methods.hooks");

module.exports = function(app) {
  const paginate = app.get("paginate");

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use("/technician-methods", createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service("technician-methods");

  service.hooks(hooks);
};
