const fs = require("fs");
const jwt = require("jsonwebtoken");
const generate = require("nanoid/generate");
const mime = require("mime");
const alphabet =
  "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

module.exports = function(app, socket) {
  //create necessary folders
  if (!fs.existsSync("./uploads")) {
    fs.mkdirSync("./uploads");
  }
  const uploadFolders = ["farmer"];
  uploadFolders.map(folder => {
    if (!fs.existsSync(`./uploads/${folder}`)) {
      fs.mkdirSync(`./uploads/${folder}`);
    }
  });

  const files = {};
  const SLICE_SIZE = 100000;

  setInterval(() => {
    // delete inactive files
    Object.keys(files).map(fileId => {
      const lastUpdated = files[fileId].lastUpdated;
      const timeNow = new Date().getTime();
      if (timeNow - lastUpdated > 60000) {
        delete files[fileId];
      }
    });
  }, 60000);

  socket.on("file upload new", async data => {
    const { token } = data;
    if (!token) {
      socket.emit("file error", { error: "not authenticated" });
      return;
    }
    const authentication = app.get("authentication");
    let decoded;
    try {
      decoded = jwt.verify(token, authentication.secret);
    } catch (err) {
      socket.emit("file error", { error: "invalid token" });
      return;
    }
    const { userId } = decoded;
    const { uploadType, uploadTypeId, file } = data;
    if (!uploadType || !uploadTypeId || !file.type || !file.size) {
      socket.emit("file error", { error: "missing parameters" });
      return;
    }
    //type here refers to farmerId
    switch (uploadType) {
      case "farmerId":
        //must have uploadToken
        try {
          decoded = jwt.verify(data.uploadToken, authentication.secret);
        } catch (err) {
          socket.emit("file error", { error: "invalid upload token" });
          return;
        }
        break;
      default:
        break;
    }

    const fileId = generate(alphabet, 21);

    files[fileId] = {
      name: file.name ? file.name : fileId,
      type: file.type,
      size: file.size,
      data: [],
      slice: 0,
      sliceSize: SLICE_SIZE,
      user: { id: userId },
      uploadType,
      uploadTypeId: decoded.uploadTypeId ? decoded.uploadTypeId : uploadTypeId,
      lastUpdated: new Date().getTime()
    };

    socket.emit(
      "file upload start",
      Object.assign({}, { fileId }, files[fileId])
    ); // client must send slice 0
  });

  socket.on("file upload slice", data => {
    const { fileId, data: fileSlice } = data;
    if (!fileId || !files[fileId]) {
      socket.emit("file error", { error: "no such file" });
      return;
    }
    files[fileId].data.push(new Buffer(new Uint8Array(fileSlice)));
    files[fileId].slice++;
    files[fileId].lastUpdated = new Date().getTime();
    if (files[fileId].slice * SLICE_SIZE >= files[fileId].size) {
      const fileBuffer = Buffer.concat(files[fileId].data);
      const fileExt = mime.getExtension(files[fileId].type);

      let fileName = "";
      switch (files[fileId].uploadType) {
        case "farmerId":
          fileName = `./uploads/farmer/${files[fileId].uploadTypeId}.jpg`;
          break;
      }

      fs.writeFile(fileName, fileBuffer, async err => {
        const fileInfo = files[fileId];
        delete files[fileId];
        if (err) {
          socket.emit("file error", { error: err });
          return;
        }
        //TODO: perform addition of image to the relevant fields depending on type
        switch (fileInfo.uploadType) {
          case "farmerId":
            break;
          default:
            break;
        }

        // End
        socket.emit("file upload end", { fileId, results: { fileId } });
      });
    } else {
      socket.emit("file upload continue", {
        fileId,
        slice: files[fileId].slice,
        sliceSize: SLICE_SIZE
      });
    }
  });
};
