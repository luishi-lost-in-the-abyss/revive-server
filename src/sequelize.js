const locationInitial = require("../data/locations/initial");
const locationUpdateOne = require("../data/locations/290719");
const bookingsUpdate = require("../data/booking_updates/index");
const csv = require("csvtojson");
const iconvlite = require("iconv-lite");
const Sequelize = require("sequelize");
const { Op } = Sequelize;
const operatorsAliases = {
  $eq: Op.eq,
  $ne: Op.ne,
  $gte: Op.gte,
  $gt: Op.gt,
  $lte: Op.lte,
  $lt: Op.lt,
  $not: Op.not,
  $in: Op.in,
  $notIn: Op.notIn,
  $is: Op.is,
  $like: Op.like,
  $notLike: Op.notLike,
  $iLike: Op.iLike,
  $notILike: Op.notILike,
  $regexp: Op.regexp,
  $notRegexp: Op.notRegexp,
  $iRegexp: Op.iRegexp,
  $notIRegexp: Op.notIRegexp,
  $between: Op.between,
  $notBetween: Op.notBetween,
  $overlap: Op.overlap,
  $contains: Op.contains,
  $contained: Op.contained,
  $adjacent: Op.adjacent,
  $strictLeft: Op.strictLeft,
  $strictRight: Op.strictRight,
  $noExtendRight: Op.noExtendRight,
  $noExtendLeft: Op.noExtendLeft,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col
};

module.exports = function(app) {
  const connectionString = app.get("mysql");
  const sequelize = new Sequelize(connectionString, {
    dialect: "mysql",
    logging: false,
    operatorsAliases,
    define: {
      freezeTableName: true
    }
  });
  const oldSetup = app.setup;

  app.set("sequelizeClient", sequelize);

  app.setup = function(...args) {
    const result = oldSetup.apply(this, args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach(name => {
      if ("associate" in models[name]) {
        models[name].associate(models);
      }
    });

    // Sync to the database
    sequelize.sync({ force: false, alter: false }).then(async function() {
      //initial location update
      try {
        await locationInitial({
          app,
          update: locationUpdateOne.bind(null, { app })
        });
      } catch (err) {
        console.log(err);
      }

      // Add admin user if does not exist
      app
        .service("users")
        .find({ query: { role: "super_admin" } })
        .then(async res => {
          if (res.total === 0) {
            app.service("users").create({
              username: "admin",
              email: "qixian@leeqixian.com",
              password: "admin",
              role: "super_admin",
              status: "activated"
            });
            const fec = await app.service("fec").create({ name: "BANGA" });
            await app.service("fec").create({ name: "TANTANGAN" });

            const supervisor = await app.service("admin-methods").create({
              method: "create_supervisor",
              query: {
                name: "David Tan",
                username: "supervisor",
                email: "supervisor@leeqixian.com",
                password: "123456",
                fec: fec.id
              }
            });
            await app.service("admin-methods").create({
              method: "create_technician",
              query: {
                name: "Peter Lee",
                username: "technician",
                email: "technician@leeqixian.com",
                password: "123456",
                barangayId: 100,
                supervisor: supervisor.id
              }
            });
          }
        });

      // setup basic category, service and equipment
      app
        .service("services")
        .find({})
        .then(async res => {
          if (res.total > 0) return;

          const equipmentFile = await csv().fromFile("./data/equipment.csv");
          const wetServiceFile = await csv().fromFile(
            "./data/wetland-service-codes.csv"
          );
          const dryServiceFile = await csv().fromFile(
            "./data/dryland-service-codes.csv"
          );

          //create equipment first
          let prevEquipment;
          let equipmentAssetObj = {};
          equipmentFile.map(({ name, description, serial, quantity }) => {
            if (!name) {
              equipmentAssetObj[
                prevEquipment.name + "-" + prevEquipment.description
              ].assets.push({
                serial,
                quantity
              });
            } else {
              prevEquipment = { name, description };
              equipmentAssetObj[name + "-" + description] = {
                description,
                assets: [{ serial, quantity }]
              };
            }
          });
          let i = 0;
          const allEquipment = equipmentFile.map(file => {
            if (!file.name) return null;
            const name = file.name + "-" + file.description;
            const { description, assets } = equipmentAssetObj[name];
            i += 1;
            return app
              .service("equipment")
              .create({ id: i, name: name.split("-")[0], description })
              .then(res => {
                assets.map(({ serial, quantity }) => {
                  const arr = [];
                  for (let i = 1; i <= parseInt(quantity); i++) {
                    arr.push({
                      serial: `${serial}-${i}`,
                      fecId: 1,
                      equipmentId: res.id,
                      condition: "working"
                    });
                  }
                  app.service("equipment-assets").create(arr);
                });
              });
          });
          await Promise.all(allEquipment);

          let dryService = await app
            .service("service-category")
            .create({ name: "Dryland" });
          let wetService = await app
            .service("service-category")
            .create({ name: "Wetland" });

          const servicePhases = [
            "Soil Preparation",
            "Planting",
            "Maintenance",
            "Harvesting"
          ];
          dryService = await app.service("service-category").create(
            servicePhases.map(phase => ({
              name: phase,
              parentCategoryId: dryService.id
            }))
          );
          wetService = await app.service("service-category").create(
            servicePhases.map(phase => ({
              name: phase,
              parentCategoryId: wetService.id
            }))
          );
          const dryServicePhases = {
            preparation: dryService[0],
            planting: dryService[1],
            maintenance: dryService[2],
            harvesting: dryService[3]
          };
          const wetServicePhases = {
            preparation: wetService[0],
            planting: wetService[1],
            maintenance: wetService[2],
            harvesting: wetService[3]
          };

          const wetServices = wetServiceFile
            .map(({ name, code, category, equipment, implement }) => ({
              code,
              name,
              description: "",
              price: 200,
              equipment: [equipment, implement].filter(a => !!a),
              serviceCategoryId: wetServicePhases[category].id
            }))
            .map(obj => {
              return app.service("services").create(obj);
            });
          const dryServices = dryServiceFile
            .map(({ name, code, category, equipment, implement }) => ({
              code,
              name,
              description: "",
              price: 200,
              equipment: [equipment, implement].filter(a => !!a),
              serviceCategoryId: dryServicePhases[category].id
            }))
            .map(obj => {
              return app.service("services").create(obj);
            });
          await Promise.all(wetServices);
          await Promise.all(dryServices);
        });

      //FOR COMPATIBILITY TO GET SNAPSHOT FOR THOSE BOOKINGS THAT HAVE YET ADDED THE DATA SNAPSHOT
      app
        .service("bookings")
        .find({ paginate: false, query: { dataSnapshot: null } })
        .then(async bookings => {
          const convertBookings = bookings.map(async booking => {
            participants = booking.farm_plot.formal_cluster
              ? [
                  await app
                    .service("formal-clusters")
                    .get(booking.farm_plot.formal_cluster.id)
                ]
              : await app.service("participants").find({
                  paginate: false,
                  query: booking.farm_plot.participant.clusterId
                    ? {
                        clusterId: booking.farm_plot.participant.clusterId
                      }
                    : { id: booking.farm_plot.participant.id }
                });
            return app.service("bookings").patch(booking.id, {
              dataSnapshot: { booking, participants }
            });
          });

          await Promise.all(convertBookings);
        });

      if (app.get("loadParticipants")) {
        //load up participants
        const userNameCheck = {};
        const participantFile = await csv().fromFile("./data/participants.csv");

        const getParticipantGender = gender => {
          switch (gender.toLowerCase()) {
            case "male":
              return "m";
            case "female":
              return "f";
            default:
              return "m";
          }
        };
        for (i in participantFile) {
          const participant = participantFile[i];
          const {
            No,
            Type,
            EngagementScheme,
            ...formatParticipant
          } = participant;
          if (userNameCheck[participant.username]) {
            console.log("Duplicate", participant.username);
            return;
          } else {
            userNameCheck[participant.username] = true;
          }
          app.service("users").create(
            Object.assign({}, formatParticipant, {
              firstName: iconvlite.encode(participant.firstName, "win1251"),
              middleName: iconvlite.encode(participant.middleName, "win1251"),
              lastName: iconvlite.encode(participant.lastName, "win1251"),
              bankAccName: iconvlite.encode(participant.bankAccName, "win1251"),
              username: iconvlite.encode(participant.username, "win1251"),
              residentAddress: iconvlite.encode(
                participant.residentAddress,
                "win1251"
              ),
              type: participant.Type.toLowerCase(),
              engagementScheme: participant.EngagementScheme.toLowerCase(),
              birthdate: participant.birthdate
                ? new Date(participant.birthdate)
                : new Date(),
              civil: participant.civil.toLowerCase(),
              contact:
                participant.contact === "-"
                  ? "-"
                  : "+63 " +
                    participant.contact
                      .slice(1)
                      .split("-")
                      .join(""),
              operator: participant.operator.toLowerCase(),
              gender: getParticipantGender(participant.gender),
              healthConditions: [],
              email:
                participant.email.split("@").length !== 2
                  ? ""
                  : participant.email,
              role: "participant",
              status: "activated",
              plots: [],
              socio: {
                spouseName: "",
                spouseOccupation: ""
              },
              beneficiary: []
            })
          );
        }
      }
      //initial location update
      try {
        await bookingsUpdate({ app });
      } catch (err) {
        console.log(err);
      }
    });

    return result;
  };
};
