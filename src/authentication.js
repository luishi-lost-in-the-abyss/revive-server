const authentication = require("@feathersjs/authentication");
const jwt = require("@feathersjs/authentication-jwt");
const local = require("@feathersjs/authentication-local");
const Verifier = require("@feathersjs/authentication-local/lib/verifier");
const JWTVerifier = require("@feathersjs/authentication-jwt/lib/verifier");
const { get, omit } = require("lodash");
const Debug = require("debug");
const debug = Debug("@feathersjs/authentication-local:verify");

class CustomVerifier extends Verifier {
  verify(req, username, password, done) {
    const id = this.service.id;
    const usernameField =
      this.options.entityUsernameField || this.options.usernameField;
    const params = Object.assign(
      {
        query: {
          [usernameField]: username,
          $limit: 1
        }
      },
      omit(req.params, "query", "provider", "headers", "session", "cookies")
    );

    if (id === null || id === undefined) {
      debug("failed: the service.id was not set");
      return done(
        new Error(
          "the `id` property must be set on the entity service for authentication"
        )
      );
    }

    this.service
      .find(params)
      .then(response => {
        const results = response.data || response;
        if (!results.length) {
          debug(
            `a record with ${usernameField} of '${username}' did not exist`
          );
        }
        return this._normalizeResult(response);
      })
      .then(entity => this._comparePassword(entity, password))
      .then(entity => {
        //check if entity is activated
        if (entity.status !== "activated")
          throw new Error("account not activated");
        const id = entity[this.service.id];
        const payload = { [`${this.options.entity}Id`]: id, role: entity.role };
        done(null, entity, payload);
      })
      .catch(error =>
        error ? done(error) : done(null, error, { message: "Invalid login" })
      );
  }
}

class CustomJWTVerifier extends JWTVerifier {
  verify(req, payload, done) {
    debug("Received JWT payload", payload);
    const id = payload[`${this.options.entity}Id`];

    if (id === undefined) {
      debug(`JWT payload does not contain ${this.options.entity}Id`);
      return done(null, {}, payload);
    }

    debug(`Looking up ${this.options.entity} by id`, id);

    this.service
      .get(id)
      .then(entity => {
        //check if entity is activated
        if (entity.status !== "activated")
          throw new Error("account not activated");

        const newPayload = {
          [`${this.options.entity}Id`]: id,
          role: entity.role
        };
        return done(null, entity, newPayload);
      })
      .catch(error => {
        debug(`Error populating ${this.options.entity} with id ${id}`, error);
        return done(error);
      });
  }
}

module.exports = function(app) {
  const config = app.get("authentication");

  // Set up authentication with the secret
  app.configure(authentication(config));
  app.configure(jwt({ Verifier: CustomJWTVerifier }));
  app.configure(local({ Verifier: CustomVerifier }));

  // The `authentication` service is used to create a JWT.
  // The before `create` hook registers strategies that can be used
  // to create a new valid JWT (e.g. local or oauth2)
  app.service("authentication").hooks({
    before: {
      create: [authentication.hooks.authenticate(config.strategies)],
      remove: [authentication.hooks.authenticate("jwt")]
    },
    after: {
      create: [
        function(context) {
          context.result.role = context.params.payload.role;
        }
      ]
    }
  });
};
