module.exports = function(context) {
  const Barangay = context.app.services["location-barangay"].Model;
  const Municipality = context.app.services["location-municipality"].Model;
  const Province = context.app.services["location-province"].Model;
  const Region = context.app.services["location-region"].Model;
  const Country = context.app.services["location-country"].Model;
  return {
    model: Barangay,
    as: "barangay",
    include: [
      {
        model: Municipality,
        as: "municipality",
        include: [
          {
            model: Province,
            as: "province",
            include: [
              {
                model: Region,
                as: "region",
                include: [{ model: Country, as: "country" }]
              }
            ]
          }
        ]
      }
    ]
  };
};
