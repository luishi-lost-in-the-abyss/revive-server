const errors = require("@feathersjs/errors");

module.exports = function(roleTable = {}) {
  return function(context) {
    if (roleTable.server) return true;
    const hookProvider = (context.params || {}).provider;
    if (!hookProvider) {
      //this call is from internal server, should just allow
      return false;
    }
    if (
      !context.params ||
      !context.params.payload ||
      !context.params.payload.role
    ) {
      return false;
    }
    const role = context.params.payload.role;
    if (roleTable[role]) return true;

    return false;
  };
};
