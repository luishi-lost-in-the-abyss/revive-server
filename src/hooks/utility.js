const errors = require("@feathersjs/errors");
const { registrationValues } = require("../api/data-values");

module.exports = {
  validateFarmPlots: function(plots) {
    const {
      modeOfPayment,
      tenureStatus,
      cropType,
      waterSystem,
      financialSource
    } = registrationValues;
    plots.map(plot => {
      // validating data entered for plots
      // TODO: VALIDATE LOCATION
      const {
        tenure,
        commodity,
        waterSystem: inputWaterSystem,
        prevPaymentMode,
        sourceFinance,
        totalArea,
        plantableArea
      } = plot;
      if (
        tenureStatus.indexOf(tenure) < 0 ||
        modeOfPayment.indexOf(prevPaymentMode) < 0 ||
        cropType.indexOf(commodity) < 0 ||
        waterSystem.indexOf(inputWaterSystem) < 0 ||
        financialSource.indexOf(sourceFinance) < 0 ||
        !totalArea ||
        !plantableArea
      ) {
        throw new errors.BadRequest("invalid farm plot");
      }
    });
  }
};
