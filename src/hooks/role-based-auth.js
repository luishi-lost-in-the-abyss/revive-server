const errors = require("@feathersjs/errors");

module.exports = function(roleTable = {}) {
  return function(context) {
    const hookProvider = (context.params || {}).provider;
    if (!hookProvider) {
      //this call is from internal server, should just allow
      return context;
    }
    if (
      !context.params ||
      !context.params.payload ||
      !context.params.payload.role
    ) {
      throw new Error("Payload role cannot be empty");
    }
    const role = context.params.payload.role;
    if (roleTable[role]) return context;

    throw new errors.Forbidden("Not Allowed");
  };
};
