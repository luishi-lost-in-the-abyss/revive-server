const LogoBase64 = require("./LogoBase64");
module.exports = {
  pageSize: { width: 612, height: 792 },
  header: {
    columns: [
      {
        width: "auto",
        stack: [
          {
            image: LogoBase64,
            width: 121,
            height: 50
          }
        ]
      },
      { width: "*", text: "" },
      {
        width: "200",
        stack: [
          { text: "Revive Croptech Inc.", bold: true },
          {
            stack: ["Brgy. Dumadalig, Tantangan", "South Cotabato"],
            style: "headerColumnSub"
          }
        ],
        style: "headerColumn"
      }
    ],
    style: "divHeader"
  },

  contactDetails: [
    { text: "Contact details:", bold: true, margin: [0, 0, 0, 5] },
    "Email: admin.staff01@revive-agritech.com",
    "Mobile Number: 0917-593-1111",
    "Phone Number: (083) 228 2315 local 2518"
  ],
  footerNotes: [
    { text: "Notes:", margin: [0, 20, 0, 0], style: "subtitle" },
    {
      ul: [
        {
          text: "All payments include transport fees.",
          listType: "circle"
        },
        {
          text:
            "Requester must be present during requested date and time before serving the booked schedule. In case not available, representative's full name must be relayed to technician beforehand and he/she must bring the requester's valid id together with the service order.",
          listType: "circle"
        },
        {
          text:
            "Requester or representative shall be allotted maximum of 10 mins extension to arrive in the area or else service order will be cancelled and be listed on the blacklist for a year as sanction.",
          listType: "circle"
        }
      ],
      style: "subtitle"
    }
  ],
  extractLocationDetails: barangayObj => {
    const barangay = barangayObj;
    const municipality = barangay.municipality;
    const province = municipality.province;
    const region = province.region;
    const country = region.country;
    return { barangay, municipality, province, region, country };
  }
};
