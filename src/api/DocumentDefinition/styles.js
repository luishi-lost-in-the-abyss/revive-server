module.exports = {
  divHeader: {
    marginBottom: 10
  },
  headerColumn: {
    marginTop: 20,
    marginLeft: 25,
    alignment: "left"
  },
  headerColumnSub: {
    fontSize: 8
  },
  title: {
    fontSize: 12,
    bold: true
  },
  subtitle: {
    fontSize: 8
  },
  subtitleMeta: {
    fontSize: 10,
    color: "#a6a6a6"
  },
  bold: {
    bold: true
  },
  smallText: {
    fontSize: 8
  },
  header: {
    fontSize: 12,
    bold: true,
    margin: [0, 10, 0, 5]
  },
  footer: {
    fontSize: 8,
    margin: [0, 40, 0, 0]
  },
  footerText: {
    fontSize: 8
  },
  tableHeader: {
    fillColor: "#1B5E20",
    color: "white",
    bold: true,
    alignment: "center"
  },
  defaultStyle: {
    fontSize: 10
  }
};
