const styles = require("./styles");
const { DateTime } = require("luxon");
const {
  pageSize,
  header: commonHeader,
  contactDetails,
  footerNotes,
  extractLocationDetails
} = require("./common");

module.exports = ({ farm_plots, booking, isInformal = false }) => {
  const technician = booking.technician
    ? booking.technician.name
    : "SYSTEM GENERATED";
  const isFormalCluster = !!farm_plots[0].formal_cluster;
  const participant = isFormalCluster
    ? farm_plots[0].formal_cluster
    : farm_plots[0].participant;
  const service = booking.service;
  const discount = booking.discount ? booking.discount : 0;
  const totalPlantableArea = isInformal
    ? farm_plots.reduce((a, { plantableArea }) => a + plantableArea, 0)
    : booking.serviceArea;
  const additionalCosts = booking.additionalCosts
    ? booking.additionalCosts.reduce(
        (a, { amount }) => a + parseFloat(amount),
        0
      )
    : 0;
  const costBeforeDiscount = parseFloat(
    additionalCosts + totalPlantableArea * service.price
  ).toFixed(2);
  const costAfterDiscount = parseFloat(
    (1 - discount / 100) * costBeforeDiscount
  ).toFixed(2);

  const {
    barangay,
    municipality,
    province,
    region,
    country
  } = extractLocationDetails(farm_plots[0].barangay);
  const purok = farm_plots[0].purok;
  const commodity = farm_plots[0].commodity;

  const documentHeader = [
    commonHeader,
    {
      columns: [
        {
          width: "auto",
          stack: [
            {
              text: `SERVICE ORDER`,
              style: "title"
            },
            {
              text: `Service Order RE-SO-${booking.id}${
                isInformal ? `-${participant.id}` : ""
              }`,
              style: "subtitle"
            },
            {
              text: `Order Date: ${DateTime.fromJSDate(
                new Date(booking.createdAt)
              ).toFormat("d/M/yyyy")}`,
              style: "subtitleMeta"
            },
            {
              text: `Booking ID: ${booking.id}`
            },
            {
              text: `Commodity: ${commodity}`
            },
            {
              text: `Farm Technician: ${technician}`
            }
          ],
          style: "subtitle"
        },
        { width: "*", text: "" },
        {
          width: "175",
          stack: contactDetails,
          style: "headerColumnSub"
        }
      ],
      style: "divHeader"
    },

    {
      columns: [
        {
          width: "auto",
          stack: [
            { text: "Ordered / Agreed by:", style: ["bold"] },
            `${participant.name}`,
            isFormalCluster
              ? participant.cluster.code
              : `${participant.residentAddress}`,
            `${participant.contact}`,
            {
              margin: [0, 20, 0, 0],
              columns: [
                {
                  text: "Mode of Payment:",
                  bold: true,
                  border: [false, false, false, false],
                  style: "subtitle"
                },
                {
                  margin: [0, -3, 0, 0],
                  table: {
                    widths: [75],
                    body: [
                      [
                        {
                          text: " ",
                          border: [false, false, false, true]
                        }
                      ]
                    ]
                  }
                }
              ],
              style: ["subtitle"]
            }
          ]
        },
        { width: "*", text: "" },
        {
          margin: [0, -15, 0, 10],
          width: 175,
          stack: [
            {
              text: "Delivery To:",
              style: ["bold"]
            },
            isFormalCluster
              ? participant.name
              : booking.farm_plot.participant.name +
                (isInformal ? " (Cluster Leader)" : ""),
            country.name,
            region.name,
            province.name,
            municipality.name,
            barangay.name,
            purok,
            `PLOT CODE: ${booking.farm_plot.code}`
          ]
        }
      ],
      style: "subtitle"
    }
  ];

  const documentContent = [
    {
      table: {
        headerRows: 1,
        widths: ["*", "auto", "auto", "auto"],
        body: [
          ["Service Description", "Cost / ha", "Area (ha)", "Amount"].map(
            text => ({ text, style: "tableHeader" })
          ),
          ...farm_plots.map(plot => {
            const serviceArea =
              !isInformal && farm_plots.length === 1
                ? booking.serviceArea
                : plot.plantableArea;
            return [
              {
                stack: [
                  `${service.code} ${service.name}`,
                  plot.code,
                  `From: ${DateTime.fromJSDate(
                    new Date(booking.startDate)
                  ).toFormat("d/M/yyyy H:mm")}`,
                  `To: ${DateTime.fromJSDate(
                    new Date(booking.endDate)
                  ).toFormat("d/M/yyyy H:mm")}`
                ]
              },
              parseFloat(service.price).toFixed(2),
              serviceArea,
              {
                text: (
                  parseFloat(service.price) * parseFloat(serviceArea)
                ).toFixed(2)
              }
            ];
          }),
          ...(booking.additionalCosts
            ? booking.additionalCosts.map(({ amount, type }) => {
                return [type, "-", "-", { text: parseFloat(amount) }];
              })
            : []),
          [
            {
              colSpan: 3,
              text: "Sub Total",
              alignment: "right",
              bold: true
            },
            {},
            {},
            costBeforeDiscount
          ],
          [
            {
              colSpan: 3,
              text: "Discount (%)",
              alignment: "right",
              bold: true
            },
            {},
            {},
            discount
          ],
          [
            {
              colSpan: 3,
              text: "Total",
              alignment: "right",
              bold: true
            },
            {},
            {},
            costAfterDiscount
          ]
        ]
      },
      style: "subtitle"
    }
  ];

  const documentFooter = [
    ...footerNotes,

    {
      columns: [
        {
          width: "*",
          stack: [
            {
              text: "Ordered/Agreed By:",
              italics: true,
              margin: [0, 0, 0, 30],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        },
        { width: 100, text: "" },
        {
          width: "*",
          stack: [
            {
              text: "Recommended By:",
              italics: true,
              margin: [0, 0, 0, 30],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        }
      ],
      margin: [0, 40, 0, 0]
    },
    {
      columns: [
        { width: 150, text: "" },
        {
          width: "*",
          stack: [
            {
              text: "Noted By:",
              italics: true,
              margin: [0, 0, 0, 30],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        },
        { width: 150, text: "" }
      ],
      margin: [0, 40, 0, 0]
    }
  ];

  return {
    pageSize,
    content: [...documentHeader, ...documentContent, ...documentFooter],
    styles
  };
};
