const styles = require("./styles");
const { DateTime } = require("luxon");
const {
  pageSize,
  header: commonHeader,
  contactDetails,
  footerNotes,
  extractLocationDetails
} = require("./common");

module.exports = ({ farm_plots, booking, isInformal = false }) => {
  const technician = booking.technician
    ? booking.technician.name
    : "SYSTEM GENERATED";
  const isFormalCluster = !!farm_plots[0].formal_cluster;
  const participant = isFormalCluster
    ? farm_plots[0].formal_cluster
    : farm_plots[0].participant;
  const service = booking.service;
  const discount = booking.discount ? booking.discount : 0;
  const totalPlantableArea = booking.actualCompletedArea
    ? booking.actualCompletedArea
    : isInformal
    ? farm_plots.reduce((a, { plantableArea }) => a + plantableArea, 0)
    : booking.serviceArea;
  const additionalCosts = booking.additionalCosts
    ? booking.additionalCosts.reduce(
        (a, { amount }) => a + parseFloat(amount),
        0
      )
    : 0;
  const costBeforeDiscount = parseFloat(
    additionalCosts + totalPlantableArea * service.price
  ).toFixed(2);
  const costAfterDiscount = parseFloat(
    (1 - discount / 100) * costBeforeDiscount
  ).toFixed(2);

  const commodity = farm_plots[0].commodity;
  const plotCode = farm_plots[0].code;

  const documentHeader = [
    commonHeader,
    {
      columns: [
        {
          width: "auto",
          stack: [
            {
              text: `SERVICE INVOICE`,
              style: "title"
            },
            {
              text: `Service Invoice RE-INV-${booking.id}${
                isInformal ? `-${participant.id}` : ""
              }`,
              style: "subtitle"
            },
            {
              text: `Service Invoice Date: ${DateTime.fromJSDate(
                new Date(booking.startDate)
              ).toFormat("d/M/yyyy")}`,
              style: "subtitleMeta"
            },
            {
              text: `Booking ID: ${booking.id}`
            },
            {
              text: `Commodity: ${commodity}`
            },
            {
              text: `Farm Technician: ${technician}`
            }
          ],
          style: "subtitle"
        },
        { width: "*", text: "" },
        {
          width: "175",
          stack: contactDetails,
          style: "headerColumnSub"
        }
      ],
      style: "divHeader"
    },
    {
      columns: [
        {
          margin: [0, -10, 0, 10],
          width: "*",
          stack: [
            { text: "Invoiced To:", style: ["bold", "subtitle"] },
            `${participant.name}`,
            isFormalCluster
              ? participant.cluster.code
              : `${participant.residentAddress}`,
            `PLOT CODE: ${plotCode}`,
            `${participant.contact}`
          ],
          style: ["subtitle"]
        },
        {
          width: 175,
          columns: [
            {
              text: "Mode of Payment:",
              bold: true,
              border: [false, false, false, false],
              style: "subtitle"
            },
            {
              margin: [0, -3, 0, 0],
              table: {
                widths: [75],
                body: [
                  [
                    {
                      text: " ",
                      border: [false, false, false, true]
                    }
                  ]
                ]
              }
            }
          ],
          style: ["subtitle"]
        }
      ]
    }
  ];

  const documentContent = [
    {
      table: {
        headerRows: 1,
        widths: ["*", "auto", "auto", "auto"],
        body: [
          ["Service Description", "Cost / ha", "Area (ha)", "Amount"].map(
            text => ({ text, style: "tableHeader" })
          ),
          ...farm_plots.map(plot => {
            const serviceArea =
              !isInformal && farm_plots.length === 1
                ? booking.actualCompletedArea
                  ? booking.actualCompletedArea
                  : booking.serviceArea
                : plot.plantableArea;
            return [
              {
                stack: [
                  `${service.code} ${service.name}`,
                  plot.code,
                  `From: ${DateTime.fromJSDate(
                    new Date(
                      booking.actualStartDate
                        ? booking.actualStartDate
                        : booking.startDate
                    )
                  ).toFormat("d/M/yyyy H:mm")}`,
                  `To: ${DateTime.fromJSDate(
                    new Date(
                      booking.actualEndDate
                        ? booking.actualEndDate
                        : booking.endDate
                    )
                  ).toFormat("d/M/yyyy H:mm")}`
                ]
              },
              parseFloat(service.price).toFixed(2),
              serviceArea,
              {
                text: (
                  parseFloat(service.price) * parseFloat(serviceArea)
                ).toFixed(2)
              }
            ];
          }),
          ...(booking.additionalCosts
            ? booking.additionalCosts.map(({ amount, type }) => {
                return [
                  type,
                  "-",
                  "-",
                  { text: parseFloat(amount).toFixed(2) }
                ];
              })
            : []),
          [
            {
              colSpan: 3,
              text: "Sub Total",
              alignment: "right",
              bold: true
            },
            {},
            {},
            costBeforeDiscount
          ],
          [
            {
              colSpan: 3,
              text: "Discount (%)",
              alignment: "right",
              bold: true
            },
            {},
            {},
            discount
          ],
          [
            {
              colSpan: 3,
              text: "Total",
              alignment: "right",
              bold: true
            },
            {},
            {},
            costAfterDiscount
          ]
        ]
      },
      style: "subtitle"
    }
  ];

  const documentFooter = [
    // ...footerNotes,

    { text: "Remarks", style: ["subtitle"], margin: [0, 20, 0, 2] },
    {
      table: {
        widths: ["*"],
        body: [
          [
            {
              text: "\n\n\n\n\n\n",
              border: [true, true, true, true]
            }
          ]
        ]
      },
      style: ["subtitle"]
    },
    {
      columns: [
        {
          width: "*",
          stack: [
            {
              text: "Prepared By:",
              italics: true,
              margin: [0, 0, 0, 30],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME\n(Dispatcher)",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        },
        { width: 100, text: "" },
        {
          width: "*",
          stack: [
            {
              text: "Noted By:",
              italics: true,
              margin: [0, 0, 0, 30],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME\n(Accounting)",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        }
      ],
      margin: [0, 40, 0, 0]
    }
  ];

  return {
    pageSize,
    content: [...documentHeader, ...documentContent, ...documentFooter],
    styles
  };
};
