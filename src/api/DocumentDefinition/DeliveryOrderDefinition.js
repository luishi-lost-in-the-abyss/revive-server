const styles = require("./styles");
const { DateTime } = require("luxon");
const {
  pageSize,
  header: commonHeader,
  contactDetails,
  extractLocationDetails
} = require("./common");

module.exports = ({ farm_plots, booking, isInformal = false }) => {
  const technician = booking.technician
    ? booking.technician.name
    : "SYSTEM GENERATED";
  const isFormalCluster = !!farm_plots[0].formal_cluster;
  const participant = isFormalCluster
    ? farm_plots[0].formal_cluster
    : farm_plots[0].participant;
  const service = booking.service;
  const discount = booking.discount ? booking.discount : 0;
  const totalPlantableArea = isInformal
    ? farm_plots.reduce((a, { plantableArea }) => a + plantableArea, 0)
    : booking.serviceArea;
  const costBeforeDiscount = totalPlantableArea * service.price;
  const costAfterDiscount = (1 - discount / 100) * costBeforeDiscount;

  const {
    barangay,
    municipality,
    province,
    region,
    country
  } = extractLocationDetails(farm_plots[0].barangay);
  const purok = farm_plots[0].purok;
  const commodity = farm_plots[0].commodity;

  const tableNoBorder = [false, false, false, false];
  const tableAllBorder = [true, true, true, true];
  const tableColon = {
    text: ":",
    border: tableNoBorder
  };
  const tableCellSpace = {
    text: "",
    border: tableNoBorder
  };

  const documentHeader = [
    commonHeader,
    {
      columns: [
        {
          width: "auto",
          stack: [
            {
              text: `SERVICE RECEIPT`,
              style: "title"
            },
            {
              text: `Service Receipt No. RE-SR-${booking.id}${
                isInformal ? `-${participant.id}` : ""
              }`
            },
            {
              text: `Service Invoice No. RE-INV-${booking.id}${
                isInformal ? `-${participant.id}` : ""
              }`
            },
            {
              text: `Receipt Date: ${DateTime.fromJSDate(
                new Date(booking.startDate)
              ).toFormat("d/M/yyyy")}`,
              style: "subtitleMeta"
            },
            {
              text: `Booking ID: ${booking.id}`
            },
            {
              text: `Commodity: ${commodity}`
            },
            {
              text: `Farm Technician: ${technician}`
            }
          ],
          style: "subtitle"
        },
        { width: "*", text: "" },

        {
          width: "175",
          stack: contactDetails,
          style: "headerColumnSub"
        }
      ],
      style: "divHeader"
    },

    {
      columns: [
        {
          width: "auto",
          stack: [
            { text: "Bill to:", style: ["bold"] },
            `${participant.name}`,
            isFormalCluster
              ? participant.cluster.code
              : `${participant.residentAddress}`,
            `${participant.contact}`
          ]
        },
        { width: "*", text: "" },
        {
          width: 175,
          margin: [0, -35, 0, 10],
          stack: [
            { text: "Deploy To:", style: ["bold"] },
            isFormalCluster
              ? participant.name
              : booking.farm_plot.participant.name +
                (isInformal ? " (Cluster Leader)" : ""),
            country.name,
            region.name,
            province.name,
            municipality.name,
            barangay.name,
            purok,
            `PLOT CODE: ${booking.farm_plot.code}`
          ]
        }
      ],
      style: "subtitle"
    }
  ];

  const documentContent = [
    {
      table: {
        headerRows: 1,
        widths: ["auto", "*", "auto"],
        body: [
          ["Serial", "Equipment / Description", "Qty"].map(text => ({
            text,
            style: "tableHeader"
          })),
          ...booking.equipment_assets.map(asset => [
            asset.serial,
            asset.equipment.name,
            1
          ])
        ]
      },
      style: "subtitle"
    },
    {
      margin: [0, 5, 0, 5],
      table: {
        widths: ["*"],
        body: [
          [
            {
              text: "To be filled up by Operator/Driver",
              italics: true,
              fillColor: "lightgrey",
              fontColor: "grey",
              style: "smallText"
            }
          ]
        ]
      }
    },
    {
      table: {
        widths: ["*"],
        body: [
          [
            {
              text: [
                {
                  text: "Travel Time Details ",
                  italics: true,
                  bold: true
                },
                "(From Point A to Point B)"
              ],
              border: tableNoBorder,
              fillColor: "#E7E7E7",
              fontColor: "grey",
              fontSize: 7
            }
          ]
        ]
      },
      margin: [0, 0, 0, 4]
    },
    {
      columns: [
        {
          width: "*",
          stack: [
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Location From",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Time (Departure)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "hour", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "min", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "AM/PM", width: 40, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Odometer/Hourmeter",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            }
          ]
        },
        {
          width: "*",
          stack: [
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Location To",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Time (Arrival)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "hour", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "min", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "AM/PM", width: 40, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Odometer/Hourmeter",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            }
          ]
        }
      ]
    },
    {
      table: {
        widths: ["*"],
        body: [
          [
            {
              text: [
                {
                  text: "Field Operations",
                  italics: true,
                  bold: true
                }
              ],
              border: tableNoBorder,
              fillColor: "#E7E7E7",
              fontColor: "grey",
              fontSize: 7
            }
          ]
        ]
      },
      margin: [0, 0, 0, 4]
    },
    {
      columns: [
        {
          width: "*",
          stack: [
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Date (Start)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                "",
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "mm", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "dd", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "yyyy", width: 60, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Time (Start)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "hour", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "min", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "AM/PM", width: 40, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Odometer/Hourmeter",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Actual Area Served",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: [10, 10, 5, 10, 10, 20],
                        body: [
                          [
                            "",
                            "",
                            {
                              text: ".",
                              border: tableNoBorder
                            },
                            "",
                            "",
                            {
                              text: "Ha",
                              style: "subtitle",
                              border: tableNoBorder
                            }
                          ]
                        ],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            }
          ]
        },
        {
          width: "*",
          stack: [
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Date (End)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                "",
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "mm", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "dd", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "yyyy", width: 60, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Time (End)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "hour", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "min", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "AM/PM", width: 40, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Odometer/Hourmeter",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            }
          ]
        }
      ]
    },
    {
      table: {
        widths: ["*"],
        body: [
          [
            {
              text: [
                {
                  text: "Travel Time Details ",
                  italics: true,
                  bold: true
                },
                "(From Point B to Point C)"
              ],
              border: tableNoBorder,
              fillColor: "#E7E7E7",
              fontColor: "grey",
              fontSize: 7
            }
          ]
        ]
      },
      margin: [0, 0, 0, 4]
    },
    {
      columns: [
        {
          width: "*",
          stack: [
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Location From",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Time (Departure)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "hour", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "min", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "AM/PM", width: 40, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Odometer/Hourmeter",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            }
          ]
        },
        {
          width: "*",
          stack: [
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Location To",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Time (Arrival)",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      stack: [
                        {
                          table: {
                            widths: [10, 10, 5, 10, 10, 5, 10, 10],
                            body: [
                              [
                                "",
                                "",
                                tableColon,
                                "",
                                "",
                                tableCellSpace,
                                "",
                                ""
                              ]
                            ],
                            border: tableAllBorder
                          }
                        },
                        {
                          columns: [
                            { text: "hour", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "min", width: 40, alignment: "center" },
                            { text: "", width: 12 },
                            { text: "AM/PM", width: 40, alignment: "center" }
                          ],
                          style: "smallText"
                        }
                      ],
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            },
            {
              table: {
                widths: [65, "*"],
                body: [
                  [
                    {
                      text: "Odometer/Hourmeter",
                      border: tableNoBorder,
                      style: "smallText"
                    },
                    {
                      table: {
                        widths: ["*"],
                        body: [[" "]],
                        border: tableAllBorder
                      },
                      border: tableNoBorder
                    }
                  ]
                ]
              }
            }
          ]
        }
      ]
    }
  ];

  const documentFooter = [
    {
      columns: [
        {
          width: "*",
          stack: [
            {
              text: "Served By:",
              italics: true,
              margin: [0, 0, 0, 20],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME\n(Operator/Driver)",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        },
        { width: 25, text: "" },
        {
          width: "*",
          stack: [
            {
              text: "Confirmed By:",
              italics: true,
              margin: [0, 0, 0, 20],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME\n(Farmer)",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        },
        { width: 25, text: "" },
        {
          width: "*",
          stack: [
            {
              text: "Verified By:",
              italics: true,
              margin: [0, 0, 0, 20],
              style: "subtitle"
            },
            {
              table: {
                widths: ["*"],
                body: [
                  [
                    {
                      text: "SIGNATURE OVER PRINTED NAME\n(Dispatcher)",
                      border: [false, true, false, false],
                      alignment: "center",
                      style: ["smallText"]
                    }
                  ]
                ]
              }
            }
          ]
        }
      ],
      margin: [0, 20, 0, 0]
    }
  ];

  return {
    pageSize,
    content: [...documentHeader, ...documentContent, ...documentFooter],
    styles
  };
};
