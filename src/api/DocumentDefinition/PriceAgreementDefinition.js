const styles = require("./styles");
const { DateTime } = require("luxon");
const {
  pageSize,
  header: commonHeader,
  contactDetails,
  extractLocationDetails
} = require("./common");

module.exports = ({ agreement }) => {
  const { booking } = agreement;
  const participant = booking.dataSnapshot.participants[0];
  const {
    barangay,
    municipality,
    province,
    region,
    country
  } = extractLocationDetails(booking.dataSnapshot.booking.farm_plot.barangay);
  const buyingPrice = agreement.deliveredPrice - agreement.truckingPrice;
  const documentHeader = [
    commonHeader,
    {
      columns: [
        {
          width: "auto",
          stack: [
            {
              text: `PRICE AGREEMENT`,
              style: "title"
            },
            {
              text: `Price Agreement No. RE-PA-${agreement.id}`
            },
            {
              text: `Creation Date: ${DateTime.fromJSDate(
                new Date(agreement.createdAt)
              ).toFormat("d/M/yyyy")}`,
              style: "subtitleMeta"
            },
            {
              text: `Booking ID: ${booking.id}`
            }
          ],
          style: "subtitle"
        },
        { width: "*", text: "" },

        {
          width: "175",
          stack: contactDetails,
          style: "headerColumnSub"
        }
      ],
      style: "divHeader"
    },

    {
      columns: [
        {
          width: "auto",
          stack: [
            { text: "Prepared For:", style: ["bold"] },
            `${participant.firstName} ${participant.middleName} ${
              participant.lastName
            }`
          ]
        },
        { width: "*", text: "" },
        {
          width: "auto",
          stack: [
            { text: "Farm Plot Address:", style: ["bold"] },
            country.name,
            region.name,
            province.name,
            municipality.name,
            barangay.name,
            booking.dataSnapshot.booking.farm_plot.purok
          ]
        }
      ],
      style: "subtitle"
    }
  ];

  const documentContent = [
    {
      table: {
        headerRows: 1,
        widths: ["*", "*"],
        body: [
          ["Commodity", booking.dataSnapshot.booking.farm_plot.commodity],
          [
            "Mode of Delivery",
            agreement.deliveryMode === "pickup" ? "Pick-up" : "Delivered"
          ],
          ["Buying Price (Php/Kg)", buyingPrice],
          ["Trucking Price (Php/Kg)", agreement.truckingPrice]
        ]
      },
      margin: [0, 20, 0, 20],
      style: "subtitle"
    }
  ];

  const documentFooter = [
    { text: "Agreed by:", bold: true, margin: [0, 0, 0, 20] },
    {
      columns: [
        {
          width: "*",
          table: {
            widths: [200],
            body: [
              [
                {
                  stack: [
                    {
                      text: "Field Technician",
                      bold: true
                    }
                  ],
                  border: [false, true, false, false],
                  style: "subtitle"
                }
              ]
            ]
          }
        },
        {
          width: "*",
          table: {
            widths: [200],
            body: [
              [
                {
                  stack: [
                    {
                      text: "Farmer/Representative",
                      bold: true
                    }
                  ],
                  border: [false, true, false, false],
                  style: "subtitle"
                }
              ]
            ]
          }
        }
      ],
      margin: [0, 40, 0, 40]
    },
    { text: "NOTE:", bold: true, marginBottom: 10 },
    {
      text:
        "Price above is subject to possible quality discount(s) or premium after quality checking upon truck plant arrival.",
      marginBottom: 10
    },
    {
      text:
        "Ang presyo sa itaas ay posibleng magbago depende sa resulta ng pagdating sa planta.",
      bold: true,
      italics: true
    }
  ];

  return {
    pageSize,
    content: [...documentHeader, ...documentContent, ...documentFooter],
    styles
  };
};
