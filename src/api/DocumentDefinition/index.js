const getInvoiceDefinition = require("./InvoiceDefinition");
const getPurchaseOrderDefinition = require("./PurchaseOrderDefinition");
const getDeliveryDefinition = require("./DeliveryOrderDefinition");
const getPriceAgreementDefinition = require("./PriceAgreementDefinition");
module.exports = {
  getInvoiceDefinition,
  getPurchaseOrderDefinition,
  getDeliveryDefinition,
  getPriceAgreementDefinition
};
