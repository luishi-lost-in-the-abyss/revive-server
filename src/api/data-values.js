const civilStatus = [
  { label: "Single", value: "single" },
  { label: "Married", value: "married" },
  { label: "Seperated", value: "seperated" },
  { label: "Widowed", value: "widowed" },
  { label: "Divorced", value: "divorced" }
];

const cropType = [
  { label: "Corn", value: "corn" },
  { label: "Corn Silage", value: "corn_silage" },
  { label: "Rice", value: "rice" },
  { label: "Napier", value: "napier" },
  { label: "Others", value: "others" }
];

const tenureStatus = [
  { label: "Owner", value: "owner" },
  { label: "Mortgage/Lease", value: "mortgage-lease" },
  { label: "Tenant", value: "tenant" }
];

const modeOfPayment = [
  { label: "Cash to Cash", value: "cash" },
  { label: "Bank", value: "bank" }
];

const waterSystem = [
  { label: "Irrigated", value: "irrigated" },
  { label: "Rainfed", value: "rainfed" }
];

const financialSource = [
  { label: "Self Financed", value: "self" },
  { label: "Local Financier", value: "local" },
  { label: "Trader/Buyer", value: "buyer" },
  { label: "MFI/Bank", value: "bank" }
];

const cropInsurance = [
  { label: "Yes", value: true },
  { label: "No", value: false }
];

const plotDetail = {
  code: "",
  description: "",
  type: "",
  engagementScheme: "",
  country: "",
  region: "",
  province: "",
  municipality: "",
  barangay: "",
  purok: "",
  tenure: "",
  commodity: "",
  commodityOthers: "",
  totalArea: 0,
  plantableArea: 0,
  waterSystem: "",
  coordinates: "",
  prevBuyer: "",
  prevPrice: "",
  prevPaymentMode: "",
  sourceFinance: "",
  cropInsurance: false,
  cropInsuranceDetails: "",
  clusterId: ""
};

const childDetail = {
  name: "",
  education: ""
};

const medicalConditions = [
  { label: "Arthritis", value: "arthritis" },
  { label: "Hypertension", value: "hypertension" },
  { label: "Asthma", value: "asthma" },
  { label: "Diabetes", value: "diabetes" },
  { label: "Kidney Failure", value: "kidneyFailure" },
  { label: "Others", value: "others" }
];

const landArrangements = [
  { label: "'Working' Farm Owner -- Individual", value: "01" },
  { label: "'Working' / Non-land Owner Farmer", value: "02" },
  { label: "Absentee Farm Owner", value: "03" },
  { label: "Cluster of 'Working' Farm Owner - Individual", value: "04" },
  { label: "Cooperative of 'Working' Farm Owner - Individual", value: "05" },
  { label: "Cooperative Farming", value: "06" },
  { label: "Long-term Lease by REvive", value: "07" },
  { label: "Joint Venture Land", value: "08" }
];

const clusterTypes = [
  { label: "Cluster of 'Working' Farm Owner - Individual", value: "04" },
  { label: "Cooperative of 'Working' Farm Owner - Individual", value: "05" },
  { label: "Cooperative Farming", value: "06" },
  { label: "Long-term Lease by REvive", value: "07" },
  { label: "Joint Venture Land", value: "08" }
];

const mobileOperators = [
  { label: "Globe", value: "globe" },
  { label: "Smart", value: "smart" },
  { label: "MislaTel", value: "mislatel" }
];

const registrationParams = {
  childDetail,
  civilStatus,
  cropInsurance,
  cropType,
  medicalConditions,
  tenureStatus,
  modeOfPayment,
  waterSystem,
  financialSource,
  plotDetail,
  landArrangements,
  mobileOperators
};

const registrationValues = {};
Object.keys(registrationParams).map(key => {
  if (key === "childDetail" || key === "plotDetail") return;
  registrationValues[key] = registrationParams[key].map(item => item.value);
});

module.exports = {
  registrationParams,
  registrationValues,
  clusterTypes
};
