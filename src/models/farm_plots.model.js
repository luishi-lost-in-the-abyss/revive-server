// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const farmPlots = sequelizeClient.define(
    "farm_plots",
    {
      code: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false
      },
      type: {
        type: DataTypes.STRING,
        defaultValue: "fss",
        allowNull: false
      },
      engagementScheme: {
        type: DataTypes.STRING,
        allowNull: false
      },
      purok: {
        type: DataTypes.STRING,
        allowNull: false
      },
      tenure: {
        type: DataTypes.STRING,
        allowNull: false
      },
      commodity: {
        type: DataTypes.STRING,
        allowNull: false
      },
      totalArea: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
      },
      plantableArea: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
      },
      waterSystem: {
        type: DataTypes.STRING,
        allowNull: false
      },
      coordinates: {
        type: DataTypes.STRING,
        allowNull: false
      },
      prevBuyer: {
        type: DataTypes.STRING,
        allowNull: false
      },
      prevPrice: {
        type: DataTypes.STRING,
        allowNull: false
      },
      prevPaymentMode: {
        type: DataTypes.STRING,
        allowNull: false
      },
      sourceFinance: {
        type: DataTypes.STRING,
        allowNull: false
      },
      cropInsurance: {
        //null for false, yes will be filled with the crop details
        type: DataTypes.STRING,
        allowNull: true
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  farmPlots.associate = function(models) {
    const {
      participants,
      technicians,
      formal_clusters,
      clusters,
      bookings,
      location_barangay
    } = models;
    farmPlots.belongsTo(participants);
    farmPlots.belongsTo(formal_clusters);
    farmPlots.belongsTo(clusters);
    farmPlots.belongsTo(technicians);
    farmPlots.hasMany(bookings);
    farmPlots.belongsTo(location_barangay, { as: "barangay" });
  };

  return farmPlots;
};
