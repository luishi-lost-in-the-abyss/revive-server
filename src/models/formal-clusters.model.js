// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const formalClusters = sequelizeClient.define(
    "formal_clusters",
    {
      firstName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      middleName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      contact: {
        type: DataTypes.STRING,
        allowNull: false
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: false
      },
      operator: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bankName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bankAccType: {
        type: DataTypes.STRING,
        allowNull: true
      },
      bankAccName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bankTinNo: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  formalClusters.associate = function(models) {
    const { users, participants, clusters, farm_plots } = models;
    formalClusters.belongsTo(clusters);
    formalClusters.belongsTo(users);
    formalClusters.hasMany(farm_plots);
    formalClusters.hasMany(participants);
  };

  return formalClusters;
};
