// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const clusters = sequelizeClient.define(
    "clusters",
    {
      code: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  clusters.associate = function(models) {
    const { participants, formal_clusters, farm_plots } = models;
    clusters.hasOne(participants, {
      as: "Leader",
      foreignKey: "clusterLeaderId"
    });
    clusters.hasOne(formal_clusters);
    clusters.hasMany(farm_plots);
  };

  return clusters;
};
