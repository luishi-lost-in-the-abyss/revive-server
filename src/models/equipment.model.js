// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const equipment = sequelizeClient.define(
    "equipment",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  equipment.associate = function(models) {
    const { equipment_assets, services, service_equipment } = models;
    equipment.hasMany(equipment_assets, { onDelete: "CASCADE" });
    equipment.belongsToMany(services, { through: service_equipment });
  };

  return equipment;
};
