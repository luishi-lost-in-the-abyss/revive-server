// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const equipmentAssetMaintenance = sequelizeClient.define(
    "equipment_asset_maintenance",
    {
      startDate: {
        type: DataTypes.DATE,
        allowNull: false
      },
      endDate: {
        type: DataTypes.DATE,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  equipmentAssetMaintenance.associate = function(models) {
    const { equipment_assets } = models;
    equipmentAssetMaintenance.belongsTo(equipment_assets);
  };

  return equipmentAssetMaintenance;
};
