// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const equipmentAssets = sequelizeClient.define(
    "equipment_assets",
    {
      serial: {
        type: DataTypes.STRING,
        unique: true,
        primaryKey: true,
        allowNull: false
      },
      condition: {
        type: DataTypes.STRING,
        defaultValue: "working",
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  equipmentAssets.associate = function(models) {
    const {
      equipment,
      fec,
      bookings,
      booking_equipment_assets,
      equipment_asset_maintenance
    } = models;
    equipmentAssets.belongsTo(equipment);
    equipmentAssets.belongsTo(fec);
    equipmentAssets.belongsToMany(bookings, {
      through: booking_equipment_assets
    });
    equipmentAssets.hasMany(equipment_asset_maintenance);
  };

  return equipmentAssets;
};
