// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const socioInfo = sequelizeClient.define(
    "socio_info",
    {
      data: {
        type: DataTypes.JSON,
        allowNull: true,
        defaultValue: {}
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  socioInfo.associate = function(models) {
    const { participants } = models;
    socioInfo.belongsTo(participants);
  };

  return socioInfo;
};
