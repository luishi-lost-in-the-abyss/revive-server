// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const bookings = sequelizeClient.define(
    "bookings",
    {
      startDate: {
        type: DataTypes.DATE,
        allowNull: false
      },
      endDate: {
        type: DataTypes.DATE,
        allowNull: false
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "pending"
      },
      statusReason: {
        type: DataTypes.STRING,
        allowNull: true
      },
      serviceArea: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
      },
      actualCompletedArea: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: true //if null means use service area for charging
      },
      actualStartDate: {
        type: DataTypes.DATE,
        allowNull: true
      },
      actualEndDate: {
        type: DataTypes.DATE,
        allowNull: true
      },
      additionalCosts: {
        type: DataTypes.JSON,
        allowNull: true
      },
      discount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      dataSnapshot: {
        type: DataTypes.JSON,
        allowNull: true
      },
      individualInformal: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  bookings.associate = function(models) {
    const {
      equipment_assets,
      booking_equipment_assets,
      farm_plots,
      services,
      technicians
    } = models;
    bookings.belongsToMany(equipment_assets, {
      through: booking_equipment_assets
    });
    bookings.belongsTo(farm_plots);
    bookings.belongsTo(services);
    bookings.belongsTo(technicians);
  };

  return bookings;
};
