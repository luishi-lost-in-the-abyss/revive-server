// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const serviceCategory = sequelizeClient.define(
    "service_category",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  serviceCategory.associate = function(models) {
    const { service_category, services } = models;
    serviceCategory.hasMany(services);
    serviceCategory.belongsTo(service_category, {
      as: "parent_category",
      onDelete: "CASCADE"
    });
  };

  return serviceCategory;
};
