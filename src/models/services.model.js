// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const services = sequelizeClient.define(
    "services",
    {
      code: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      price: {
        type: DataTypes.DECIMAL(13, 4),
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  services.associate = function(models) {
    const { service_category, equipment, service_equipment, bookings } = models;
    services.belongsTo(service_category);
    services.belongsToMany(equipment, {
      through: service_equipment,
      onDelete: "CASCADE"
    });
    services.hasMany(bookings);
  };

  return services;
};
