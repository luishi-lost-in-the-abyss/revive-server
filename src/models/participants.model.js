// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const participants = sequelizeClient.define(
    "participants",
    {
      firstName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      middleName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      birthdate: {
        type: DataTypes.DATEONLY,
        allowNull: false
      },
      occupation: {
        type: DataTypes.STRING,
        allowNull: false
      },
      civil: {
        type: DataTypes.STRING,
        allowNull: false
      },
      contact: {
        type: DataTypes.STRING,
        allowNull: false
      },
      operator: {
        type: DataTypes.STRING,
        allowNull: false
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: false
      },
      residentAddress: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bankName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bankAccType: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bankAccName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      bankTinNo: {
        type: DataTypes.STRING,
        allowNull: false
      },
      healthConditions: {
        type: DataTypes.JSON,
        allowNull: false,
        defaultValue: []
      },
      healthOthers: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null
      },
      direct: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  participants.associate = function(models) {
    const {
      users,
      farm_plots,
      formal_clusters,
      socio_info,
      clusters,
      technicians
    } = models;
    participants.belongsTo(users);
    participants.hasMany(farm_plots);
    participants.hasMany(socio_info);
    participants.belongsTo(formal_clusters);
    participants.belongsTo(clusters, {
      as: "Leader",
      foreignKey: "clusterLeaderId"
    });
    participants.belongsTo(technicians, { as: "createdBy" });
  };

  return participants;
};
