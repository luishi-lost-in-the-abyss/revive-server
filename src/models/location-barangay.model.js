// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const locationBarangay = sequelizeClient.define(
    "location_barangay",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  locationBarangay.associate = function(models) {
    const { location_municipality } = models;
    locationBarangay.belongsTo(location_municipality, { as: "municipality" });
  };

  return locationBarangay;
};
