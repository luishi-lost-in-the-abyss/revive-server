// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const priceAgreements = sequelizeClient.define(
    "price_agreements",
    {
      deliveredPrice: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
      },
      truckingPrice: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
      },
      deliveryMode: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  priceAgreements.associate = function(models) {
    const { bookings } = models;
    priceAgreements.belongsTo(bookings);
  };

  return priceAgreements;
};
