// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const supervisors = sequelizeClient.define(
    "supervisors",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      contact: {
        type: DataTypes.STRING,
        allowNull: true
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  supervisors.associate = function(models) {
    const { fec, users, technicians } = models;
    supervisors.belongsTo(fec);
    supervisors.belongsTo(users);
    supervisors.hasMany(technicians);
  };

  return supervisors;
};
