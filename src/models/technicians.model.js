// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const technicians = sequelizeClient.define(
    "technicians",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      contact: {
        type: DataTypes.STRING,
        allowNull: true
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  technicians.associate = function(models) {
    const { users, farm_plots, supervisors, location_barangay } = models;
    technicians.belongsTo(users);
    technicians.hasMany(farm_plots);
    technicians.belongsTo(supervisors);
    technicians.belongsTo(location_barangay, { as: "barangay" });
  };

  return technicians;
};
