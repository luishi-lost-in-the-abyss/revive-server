// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const locationProvince = sequelizeClient.define(
    "location_province",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  locationProvince.associate = function(models) {
    const { location_region, location_municipality } = models;
    locationProvince.hasMany(location_municipality, { as: "municipality" });
    locationProvince.belongsTo(location_region, { as: "region" });
  };

  return locationProvince;
};
