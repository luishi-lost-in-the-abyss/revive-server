// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const locationMunicipality = sequelizeClient.define(
    "location_municipality",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  locationMunicipality.associate = function(models) {
    const { location_barangay, location_province } = models;
    locationMunicipality.hasMany(location_barangay, { as: "barangay" });
    locationMunicipality.belongsTo(location_province, { as: "province" });
  };

  return locationMunicipality;
};
