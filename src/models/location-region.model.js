// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require("sequelize");
const DataTypes = Sequelize.DataTypes;

module.exports = function(app) {
  const sequelizeClient = app.get("sequelizeClient");
  const locationRegion = sequelizeClient.define(
    "location_region",
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true;
        }
      }
    }
  );

  // eslint-disable-next-line no-unused-vars
  locationRegion.associate = function(models) {
    const { location_country, location_province } = models;
    locationRegion.hasMany(location_province, { as: "province" });
    locationRegion.belongsTo(location_country, { as: "country" });
  };

  return locationRegion;
};
